-- MySQL dump 10.13  Distrib 5.1.58, for debian-linux-gnu (x86_64)
--
-- Host: fallen.dyndns-server.com    Database: drecker
-- ------------------------------------------------------
-- Server version	5.1.47-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `drecker`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `drecker` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `drecker`;

--
-- Table structure for table `artists`
--

DROP TABLE IF EXISTS `artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artists` (
  `ARTIST_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ARTIST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artists`
--

LOCK TABLES `artists` WRITE;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
INSERT INTO `artists` VALUES (1,'Adrian','Kim'),(2,'Adrian','Zotto'),(3,'Alberto','Aprea'),(4,'Alejandro','Machado'),(5,'Brian','Miroglio'),(6,'Damian','Graff'),(7,'Danilo','Garcia Medrano'),(8,'Dario','Reyes'),(9,'Edgar','Machiavello'),(10,'Emmanuel','Bou'),(11,'Esteban','CamposMillapell'),(12,'Fabian Alberto','Gonzales'),(13,'Fabian Mayat','Aguilar'),(14,'Felipe','Araya'),(15,'Fernando','Morales'),(16,'Franco','Riesco'),(17,'Gaston','Real'),(18,'Gaston','Zubeldia'),(19,'Julian','Nobile'),(20,'German','Nobile'),(21,'Guillermo','Villareal'),(22,'Gustavo','Santome'),(23,'Hans','Lobos'),(24,'Ignacio','Bustos'),(25,'Ivanna','Matilla'),(26,'Juan Manuel','Tumburus'),(27,'Juan Martinez','Pinilla'),(28,'Leonardo','Vallejos'),(29,'Lizzo',''),(30,'Luciano','Comizzo'),(31,'Luciano','Fleitas'),(32,'Manuela','Mauregui'),(33,'Matias','Montenegro'),(34,'MeKenzie','Martin'),(35,'Nicolas','Fernandez'),(36,'Pablo','Gorigoitia'),(37,'Paulo','Barrios'),(38,'Roberto','Viacava'),(39,'Rodolfo','Buscaglia'),(40,'Sandro','Rybak'),(41,'Santiago','Calleriza'),(42,'Sara','Villegas'),(43,'Sergio','Tarquini'),(44,'Stryke',''),(45,'Tim','Keil'),(46,'Tomas','Aira'),(47,'Veronica','Vallejos');
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
  `CARD_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `effect` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `str` varchar(255) DEFAULT NULL,
  `res` varchar(255) DEFAULT NULL,
  `EXPANSION_ID` bigint(20) DEFAULT NULL,
  `PATHWAY_ID` bigint(20) DEFAULT NULL,
  `FREQUENCY_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`CARD_ID`),
  KEY `FK3CEEF4348B7A2AA` (`PATHWAY_ID`),
  KEY `FK3CEEF43780F816A` (`FREQUENCY_ID`),
  KEY `FK3CEEF433363228A` (`EXPANSION_ID`),
  CONSTRAINT `FK3CEEF433363228A` FOREIGN KEY (`EXPANSION_ID`) REFERENCES `expansions` (`EXPANSION_ID`),
  CONSTRAINT `FK3CEEF4348B7A2AA` FOREIGN KEY (`PATHWAY_ID`) REFERENCES `pathways` (`PATHWAY_ID`),
  CONSTRAINT `FK3CEEF43780F816A` FOREIGN KEY (`FREQUENCY_ID`) REFERENCES `frequencies` (`FREQUENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=365 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (1,'Akayra, belleza ígnea','Cuando Akayra haga 5 o más daño en un turno, voltéala.','\"Mírame a los ojos. Observa tu mundo consumirse en llamas.\"','',1,'1','',1,4,1),(2,'Narghot, el irracional','Destruir cuatro recursos enderezados que controles: Voltea Narghot.','\"Destruir, necesito destruir… ¡Busquen más dominios para devastar!\"','',2,'2','',1,4,1),(3,'Syv, padre de la desgracia','Cuando controles siete o más aliados, voltea Syv.','\"La horaz ha zhegado, miz pequeñoz. ¡Ezte ez nueztro momentoz!\"','',3,'3','',1,4,1),(4,'Baggrund, agotamentes','Cuando tus enemigos no tengan cartas en su mano, voltea Baggrund.','\"Cuando no tienes opciones, no tienes futuro.\"','',4,'3','',1,3,1),(5,'Fáradt, consumevoluntades','Girar cuatro aliados enderezados que controles: Voltea Fáradt.','\"La energía proviene de cualquier lado. Algunas criaturas mediocres por ejemplo.\"','',5,'1','',1,3,1),(6,'Viden, el omnisciente','Cuando tu mazo tenga quince o menos cartas, voltea Viden.','\"Conozco el día que perecerás. Adivinaste.\"','',6,'2','',1,3,1),(7,'At Fjerne, el desterrado','Exiliar las primeras diez cartas de tu Inferno: Voltea At Fjerne.','\"Destruiré mi hogar, para encontrar uno nuevo.\"','',7,'1','',1,2,1),(8,'Naamah, escultor de carne','Pon tres aliados de tu inferno en juego, bajo el control del enemigo objetivo: Voltea Naamah.','\"Para lograr la grandeza, se necesitan hacer algunos sacrificios.\"','',8,'2','',1,2,1),(9,'Unwaj, nacida de la muerte','Cuando cinco aliados sean destruidos en un turno, voltea Unwaj.','\"Bienvenidos al Inferno.\"','',9,'3','',1,2,1),(10,'Arasunu, relámpago de vida','Cuando la FUE total de los aliados que controles sea 10 o más, voltea Arasunu.','\"¿Ves el horizonte moverse? Ya es tarde, mis bestias van por ti.\"','',10,'1','',1,1,1),(11,'Lapátt, comandate infernal','Cuando un enemigo te supere en aliados por tres o más, voltea Lapátt.','\"La fuerza de mis enemigos alimenta mi poder.\"','',11,'3','',1,1,1),(12,'Zustatek, sabio espectante','8: Voltea Zustatek.','\"Mi esencia late en los corazones de cada árbol terrenal.\"','',12,'2','',1,1,1),(13,'Arder en llamas','Tu demonio hace 2 daño a cada aliado y a cada demonio.','\"No hay mejor manera de empezar el día.\" -Akayra','3',13,'','',1,4,2),(14,'Aullido de la manada','Pon una ficha de aliado caos Kirch 1 FUE, 1 RES en juego por cada ficha que controles.','Los Kirchs sin amo vagan sin rumbo por el Gran desierto, hasta que su instinto los vuelve a reunir.','4',14,'','',1,4,3),(15,'Buscador de violencia perdida','Vínculo 0: Pon el arma objetivo de coste 3 o menos de tu Inferno en tu mano.','Poseen un vínculo poderoso con el Inferno.','2',15,'2','1',1,4,1),(16,'Caminante cordillera de fuego','Vínculo 0: Pon una ficha de aliado caos Kirch 1 FUE, 1 RES en juego.','Recorrió la Cordillera de fuego en busca de su destino, sin saber que el destino ya lo había encontrado.','3',16,'2','3',1,4,2),(17,'Carnada kirch','Agresor.','\"Hermozo hijo mioz, en tu zacrifizio ezta la victoriaz.\" -Syv','1',17,'1','1',1,4,1),(18,'Comandante nungnarh','Furia. Tus Kirch obtienen +1 FUE cuando atacan hasta el final del turno.','\"¡Avancen bestias malditas! ¡Avancen a su muerte!\"','4',18,'2','3',1,4,2),(19,'Criador de kirchs','Girar, destruir un aliado no-ficha que controles: Pon una ficha de aliado caos Kirch 1 FUE, 1 RES en juego.','“¡Quédate quieto! No me obligues a golpearte por tercera vez.”','3',19,'3','2',1,4,2),(20,'Deformidad kirch','Furia. Deformidad kirch obtiene +1 FUE por cada Kirch que controles y +1 RES por cada Kirch en tu Inferno.','Sus rugidos penetran hasta lo profundo de las cavernas.','7',20,'3','3',1,4,4),(21,'Desgracia doble','El demonio objetivo destruye una carta que controla. Recobrar 3.','\"¿Tu vida o la de tu pueblo? Más tarde volveré por la otra.\" -Narghot','3',21,'','',1,4,1),(22,'Estratega nungnarh','Siempre que un Kirch que controles ataque a un aliado, destruye ese Kirch. Estratega nungnarh hace 3 daño al aliado defensor.','\"Kirchs y explosivos. ¿Existe una mejor combinación?\"','4',22,'3','3',1,4,3),(23,'Forjadores del horno cenizo','Cuando Forjadores del horno cenizo entre en juego, busca en tu mazo un arma, muéstrala, y ponla en tu mano. Luego mezcla tu mazo.','El calor del horno cenizo es comparable solo con el mismísimo Inferno.','5',23,'5','3',1,4,3),(24,'Gemelos kirch','Pon dos fichas de aliado caos Kirch 1 FUE, 1 RES en juego. Recobrar 3.','\"Miz preziozoz hijoz… Azerquenze a mi.\" -Syv','3',24,'','',1,4,2),(25,'Guarda jaulas nungnarh','Agresor. Cuando Guarda jaulas nungnarh vaya al Inferno desde el juego, pon una ficha de aliado caos Kirch 1 FUE, 1 RES en juego por cada daño recibido.','Thakr ni siquiera tuvo tiempo para preguntarse cómo se habían abierto las jaulas.','4',25,'0','2',1,4,3),(26,'Gusano del gran desierto','Cuando Gusano del gran desierto entre en juego, hace 3 daño a todos los aliados.','\"Ten cuidado, o las arenas del Gran desierto te tragarán.\" Refrán Nungnarh','8',26,'6','6',1,4,4),(27,'Hacha de largo alcance','Cuando el aliado equipado destruya un aliado, tu demonio hace 1 daño al demonio objetivo.','La ingeniería Nungnarh es increíblemente creativa.','3',27,'2','0',1,4,2),(28,'Ignición','Tu demonio hace 2 daño al aliado o demonio objetivo.','\"Solo los que entienden la melodía del fuego, pueden disfrutar realmente del espectáculo.\" -Akayra.','1',28,'','',1,4,1),(29,'Instrumentos quirúrgicos','Destruir Instrumentos quirúrgicos: Pon el Kirch objetivo de tu Inferno en tu mano.','Ocasionalmente son herramientas de cocina y juguetes para niños.','1',29,'','',1,4,2),(30,'Kirch corazón de llama','Destruir Kirch corazón de llama: Kirch corazón de llama hace 3 daño al aliado objetivo.','Nadie ha visto uno de cerca. Al menos, nadie vivió para contarlo.','4',30,'3','1',1,4,1),(31,'Kirch entrenado','X, destruir Kirch entrenado: Destruye el arma objetivo de coste X.','\"Nunca vuelven a recibir otra orden.\" Broma Nungnarh','2',31,'1','2',1,4,1),(32,'Kirch escupefuego','Girar: Kirch escupefuego hace 1 daño al aliado objetivo.','\"Tostado. Justo como me gusta.\" -Akayra','1',32,'1','1',1,4,1),(33,'Kirch rabioso','Furia. 2, destruir Kirch rabioso: El aliado objetivo obtiene furia hasta el final del turno.','La rabia acumulada en su espíritu perdura aún después de la muerte.','2',33,'2','1',1,4,1),(34,'Lluvia de lanzas','Tu demonio hace 1 daño al aliado objetivo por cada aliado que controles.','Los guerreros Nungnarh juegan a ver cuan lejos puede llegar el prisionero.','3',34,'','',1,4,1),(35,'Sirena de voz pícara','Cuando Sirena de voz pícara entre en juego, equípale todas las armas.','\"Hoy es mi día.\"','5',35,'4','4',1,4,3),(36,'Terremoto','Cada enemigo destruye un recurso que controla. Toma una carta.','\"¿Dónde te esconderás, cuando extermine toda tierra conocida?\" -Narghot','3',36,'','',1,4,2),(37,'Tormenta de fuego','Destruye el escenario objetivo. Tu demonio hace 2 daño a su controlador.','\"Alguna vez fue un hermoso poblado.\" -Narghot','2',37,'','',1,4,1),(38,'Torre de lava','Cuando un Kirch entre en juego bajo tu control, Torre de lava hace 1 daño al aliado objetivo.','El corazón de la Cordillera de fuego no es un lugar muy visitado.','3',38,'','',1,4,3),(39,'Adelantarse a los hechos','Mira las primeras cinco cartas de tu mazo. Luego regrésalas en cualquier orden. Recobrar 1.','\"No dejes para mañana lo que puedes hacer ayer.\" -Viden','1',39,'','',1,3,1),(40,'Adivinador cyborg','Cuando Adivinador cyborg entre en juego, mira las primeras tres cartas de tu mazo. Luego regrésalas en cualquier orden.','\"Tienen una divertida manera de controlar su tiempo.\" -Ingeniero de la Esfera de cristal','2',40,'1','2',1,3,1),(41,'Asediar la mente','El demonio objetivo descarta 1 carta por cada Cyborg que controles.','\"¡Me cansé de estas tonterías, extermínenlos!\" -Baggrund.','4',41,'','',1,3,4),(42,'Bibliotecarios del éter','Cuando Bibliotecarios del éter entre en juego, busca en tu mazo un ritual, muéstralo, y ponlo en tu mano. Luego mezcla tu mazo.','\"No existe un problema que no tenga solución.\"','5',42,'3','5',1,3,3),(43,'Buscador de recuerdos perdidos','Vínculo 0: Pon el ritual objetivo de coste 3 o menos de tu Inferno en tu mano.','\"Hay recuerdos que nunca deberían recordarse.\"','2',43,'1','3',1,3,1),(44,'Cambio de planes','Cambia el/los objetivo/s del ritual o habilidad objetivo.','\"Muchas gracias. ¡Justo lo que necesitaba!\" -Ubanna','5',44,'','',1,3,3),(45,'Caminante esfera de cristal','Vínculo 0: Cada enemigo descarta una carta.','Atrás quedaron los días donde los caminantes viajaban largas distancias por nuevas noticias.','3',45,'2','3',1,3,2),(46,'Conocimiento demoníaco','Toma dos cartas. Recobrar 3.','\"¿Cuánta información puede tu débil cerebro manejar?\" -Baggrund.','3',46,'','',1,3,2),(47,'Cyborg astuto','Halo. 2, destruir Cyborg astuto: El aliado objetivo obtiene halo hasta el final del turno.','Están diseñados para no ser un problema.','2',47,'2','1',1,3,1),(48,'Cyborg de piel transparente','Evasivo.','\"¿Nunca te preguntaste por qué los líderes están siempre bien informados?\" -Guardia de la Esfera de cristal','4',48,'3','2',1,3,2),(49,'Cyborg reemplazable','X, destruir Cyborg reemplazable: Cancela el aliado objetivo de coste X.','\"Buen diseño, rápido y efectivo. Lástima su corta vida útil.\" -Ingeniero de la Esfera de cristal.','2',49,'1','2',1,3,1),(50,'Cyborg robaesencia','Cuando Cyborg robaesencia entre en juego, cancela el aliado objetivo.','Prefiere el sabor del alma antes que la carne.','4',50,'2','2',1,3,3),(51,'Deformidad cyborg','Halo. Deformidad cyborg obtiene +1 FUE por cada Cyborg que controles y +1 RES por cada Cyborg en tu Inferno.','Tiene vida, inteligencia y voluntad propia.','6',51,'4','4',1,3,4),(52,'Despistar al oponente','Gira el aliado objetivo. Toma una carta.','\"Si algo te molesta, envía un soldado cyborg.\" Broma entre guardianes','2',52,'','',1,3,2),(53,'Extensiones metálicas','Todos tus aliados obtienen halo hasta el final del turno. Vínculo 0: El aliado objetivo obtiene halo hasta el final del turno.','Durante algunos instantes, la ciudad pareció más \'organizada\' que de costumbre.','4',53,'','',1,3,3),(54,'Fábrica de reciclaje mecánico','Todos tus aliados son Cyborg además de sus otras razas. Siempre que un Cyborg no-ficha que controles vaya a un Inferno desde el juego, pon una ficha de aliado locura Cyborg 1 FUE, 1 RES en juego con protector.','Lo bueno de estos artefactos, es que saben repararse solos.','3',54,'','',1,3,3),(55,'Incapacitar','El aliado objetivo pierde todas sus habilidades hasta el final del turno. Recobrar 1.','\"Es muy fácil, solo apagas su corazón.\" -Fáradt','1',55,'','',1,3,2),(56,'Nuevas ideas','El demonio objetivo toma una carta por cada aliado que controle.','\"La mente de estos diminutos seres nunca deja de maravillarme.\" -Viden','3',56,'','',1,3,1),(57,'Rifle de asalto inteligente','Cuando el aliado equipado destruya un aliado, puedes tomar una carta.','No hace falta que sepas como usarlo, él te mostrará.','3',57,'2','0',1,3,2),(58,'Robot autoensamblante','Destruir Robot autoensamblante: Pon el Cyborg objetivo de tu Inferno en tu mano.','\"Todo se transforma.\" -Viden','1',58,'','',1,3,2),(59,'Silenciar','Mira la mano del demonio objetivo y elige una carta. Ese demonio la descarta.','\"No te escucho. ¿Me hablabas a mí?\" -Baggrund','2',59,'','',1,3,1),(60,'Sirena de voz dulce','Halo. Cuando Sirena de voz dulce entre en juego, elige un aliado objetivo. Mientras Sirena de voz dulce este en juego, ganas el control de ese aliado.','\"¿Por qué obedecer a los demonios? Yo puedo darte lo que necesitas.\"','7',60,'2','4',1,3,3),(61,'Soldado cyborg','Girar: Gira el aliado objetivo.','Eficientes modelos de combate, los soldados cyborg pueden aturdir a sus enemigos mucho antes de ser vistos.','3',61,'2','2',1,3,2),(62,'Suprimir','Cancela la carta objetivo.','Cancela la carta objetivo.','3',62,'','',1,3,1),(63,'Tejedora de mentiras','Cuando Tejedora de mentiras entre en juego, cada enemigo descarta una carta.','\"Aunque saben que estoy mintiendo, no pueden evitar creerme.\"','3',63,'2','2',1,3,1),(64,'Vigilante del cielo','Girar: Mira la primer carta de tu mazo. Puedes ponerla en la parte superior o en el fondo de tu mazo.','Conocen la Esfera de cristal mejor que ella misma.','1',64,'1','1',1,3,1),(65,'Acechadora nocturna','Evasivo.','Algunos dicen que viajan a través de nuestras sombras.','1',65,'1','1',1,2,1),(66,'Alas de sangre','El aliado objetivo que controles obtiene +2 FUE. Destrúyelo al final del turno. Recobrar 2.','\"Fuiste de gran ayuda pequeño, ahora descansa.\" -Unwaj','2',66,'','',1,2,2),(67,'Altar de expiación','Destruir Altar de expiación: Pon el Diablo objetivo de tu Inferno en tu mano.','\"Que esté enterrado no significa que esté muerto.\" -Unwaj','1',67,'','',1,2,2),(68,'Asesina daga de marfil','Girar: Destruye el aliado objetivo girado.','\"¡No te muevas! Ya nos ha visto…\" -Transeúntes entrometidos','3',68,'2','2',1,2,2),(69,'Buscador de materia perdida','Vínculo 0: Pon la carta objetivo de coste 2 o menos de tu Inferno en tu mano.','Buscan en el Inferno, lo que necesitan en la Tierra.','2',69,'1','3',1,2,1),(70,'Caminante gothika','Vínculo 0: Cada enemigo pone un aliado que controle en la mano de su demonio.','Saben diferenciar las criaturas de las sombras.','3',70,'2','3',1,2,2),(71,'Cuchillo de rito','Cuando el aliado equipado destruya un aliado, puedes exiliar la carta objetivo de un Inferno.','Ayuda a las criaturas en su camino al olvido eterno.','3',71,'2','0',1,2,2),(72,'Deformidad diablo','Evasivo. Deformidad Diablo obtiene +1 FUE por cada Diablo que controles y +1 RES por cada Diablo en tu Inferno.','','7',72,'4','2',1,2,4),(73,'Diablo de dos cabezas','Cuando Diablo de dos cabezas destruya un aliado, enderézalo.','Dos cabezas se alimentan más que una.','5',73,'3','4',1,2,3),(74,'Diablo de siete manos','X, destruir Diablo de siete manos: Destruye el aliado objetivo de coste X.','\"Hermosa creación, pero muy frágil. Deberé esforzarme más.\" -Monje iniciado','2',74,'1','2',1,2,1),(75,'Diablo intocable','Halo.','','6',75,'5','5',1,2,1),(76,'Diablo oportuno','Cuando Diablo oportuno entre en juego, exilia la carta objetivo de un Inferno.','\"Disfruto ver como mis creaciones causan dolor.\" -At Fjerne','3',76,'2','3',1,2,1),(77,'Diablo vulnerable','Cuando Diablo vulnerable sea objetivo de una carta, destrúyelo.','\"Algunas veces me decepcionas.\" -Naamah','4',77,'5','5',1,2,2),(78,'Extenuar','La FUE del aliado objetivo es 0 hasta el final del turno.','\"Te veo algo cansado…\" -Naamah','1',78,'','',1,2,1),(79,'Festival de vida nueva','Destruye todos los aliados. Cada demonio pone una ficha de aliado muerte Diablo 2 FUE, 2 RES en juego por cada aliado que controlaba destruído de esta manera.','\"Es mejor quedarse en casa.\" Dicho de Gothika','4',79,'','',1,2,4),(80,'Fisura','Exilia la carta objetivo de un Inferno. Toma una carta.','','2',80,'','',1,2,2),(81,'Gárgola diablo','Evasivo. 2, destruir Gárgola diablo: El aliado objetivo obtiene evasivo hasta el final del turno.','\"Vigilantes nocturnos, protejan nuestro majestuoso legado.\" -Unwaj','2',81,'2','1',1,2,1),(82,'Monjes de pergamino','Cuando Monjes de pergamino entre en juego, busca en tu mazo una carta y ponla en tu Inferno. Luego mezcla tu mazo.','\"Ya nos servirás luego...\"','5',82,'4','4',1,2,3),(83,'Papiro de reanimación','En tu Restauración, puedes exiliar un aliado de tu Inferno. Si lo haces, pon una ficha de aliado (MUERTE) Diablo 2 FUE, 2 RES en juego.','','4',83,'','',1,2,3),(84,'Peste negra','Tu demonio hace 2 daño al demonio objetivo por cada aliado que ese demonio controle. Vínculo 0: Tu demonio hace 2 daño a cada demonio.','El olor a muerte inundó Gothika.','5',84,'','',1,2,3),(85,'Petrificar','Destruye el aliado objetivo.','\"Los muertos huelen horrible. Es preferible otra estatua en la plaza de piedra.\" -Monje superior','3',85,'','',1,2,1),(86,'Retroceder un instante','Pon el aliado objetivo en la mano de su propietario. Recobrar 2.','\"No eres bienvenido aquí.\" -Naamah','2',86,'','',1,2,1),(87,'Ruptura infernal','Exilia el Inferno del demonio objetivo.','','4',87,'','',1,2,3),(88,'Sacrificio nocturno','Cada demonio destruye un aliado que controle.','Las mujeres más bellas a menudo realizan los ritos más crueles.','2',88,'','',1,2,1),(89,'Sirena de voz agonizante','Cuando Sirena de voz agonizante entre en juego, pon los demás aliados en la mano de sus demonios.','\"Qué lindos, me encanta cómo se arrodillan…\"','8',89,'3','5',1,2,3),(90,'Volver a la vida','Pon el aliado objetivo de tu Inferno en juego. Recobrar 4.','\"Esta es tu segunda oportunidad… Tu oportunidad de obedecerme.\" -Unwaj','4',90,'','',1,2,2),(91,'Afilar las garras','Tus aliados obtienen +2 FUE hasta el final del turno.','\"Cada ser posee una definición propia de justicia.\" -Seguidor de Arasunu','5',91,'','',1,1,3),(92,'Barahai alfa','Cuando Barahai alfa ataca, pon dos fichas de aliado poder Barahai 3 FUE, 3 RES en juego.','Sus hermanos lo seguirían hasta el exilio eterno.','8',92,'7','7',1,1,4),(93,'Barahai curioso','X, destruir Barahai curioso: Destruye el objeto objetivo de coste X.','\"¿Me pregunto qué…?\"','2',93,'1','2',1,1,1),(94,'Barahai leonino','Furia.','Aunque su rugido se escuche a varios kilómetros de distancia, seguramente ya es demasiado tarde.','4',94,'4','1',1,1,2),(95,'Barahai temerario','Furia.','Primero en defender sus dominios.','1',95,'1','1',1,1,1),(96,'Barahai valiente','Agresor. 2, destruir Barahai valiente: El aliado objetivo obtiene agresor hasta el final del turno.','Su coraje inspiró a los demás.','2',96,'2','1',1,1,1),(97,'Buscador de vida perdida','Vínculo 0: Pon el aliado objetivo de coste 3 o menos de tu Inferno en tu mano.','La vida en el Inferno tiene sus diferencias.','2',97,'2','1',1,1,1),(98,'Caminante estanque de rocío','Vínculo 0: Mezcla hasta tres cartas objetivo de tu Inferno en tu mazo.','\"Todavía hay tiempo de sanar nuestros errores.\"','3',98,'2','3',1,1,2),(99,'Cazadores del bosque','Cuando Cazadores del bosque entre en juego, busca en tu mazo un aliado, muéstralo, y ponlo en tu mano. Luego mezcla tu mazo.','El Bosque de los primeros hijos esta lleno de oportunidades.','5',99,'4','4',1,1,3),(100,'Chamán barahai','Cuando Chamán barahai entre en juego, el aliado objetivo obtiene +2 FUE hasta el final del turno.','\"Mi fuerza es ahora de mis hermanos.\"','3',100,'1','2',1,1,1),(101,'Coraje repentino','El aliado objetivo obtiene agresor hasta el final del turno.','\"Eres la clave. ¡Enorgulléceme!\" -Lapátt','1',101,'','',1,1,1),(102,'Corteza del renacimiento','Destruir Corteza del renacimiento: Pon el Barahai objetivo de tu Inferno en tu mano.','El poder de los barahai nutre al bosque, como el bosque nutre a los barahai.','1',102,'','',1,1,2),(103,'Cuchillas sagradas','Cuando el aliado equipado destruya un aliado, puedes mezclar la carta objetivo de tu Inferno en tu mazo.','La sangre que gotea de sus hojas, hace florecer incluso a los árboles secos.','3',103,'2','0',1,1,2),(104,'Deformidad barahai','Agresor. Deformidad barahai obtiene +1 FUE por cada Barahai que controles y +1 RES por cada Barahai en tu Inferno.','','6',104,'2','4',1,1,4),(105,'Fallecimiento fortuito','Destruye el aliado de menor RES (Si hay varios aliados con la menor RES, elige uno).','\"Si no conoces el camino, no te aventures en el Bosque de los primeros hijos.\" Dicho barahai','1',105,'','',1,1,1),(106,'Fuerza del bosque','El aliado objetivo obtiene +3 FUE hasta el final del turno.','\"¡Siente mi poder en tus garras!\" -Arasunu','2',106,'','',1,1,1),(107,'Hermano montado','Cuando Hermano montado ataca, obtiene +1 FUE por cada otro aliado que controlas hasta el final del turno.','La fuerza del bosque lo inspira, y las bestias luchan a su lado.','3',107,'2','2',1,1,2),(108,'Hermanos acorralados','Agresor.','\"Nara, no temas. Nuestra hora ha llegado. Los niños cantarán por siempre la canción, donde los Hermanos de la tierra protegieron su tribu.\"','3',108,'3','2',1,1,1),(109,'Primogénito del bosque','La FUE y RES de Primogénito del bosque es igual al número de recursos que controlas.','Por su savia recorre la historia de la tierra que una vez existió.','7',109,'X','X',1,1,3),(110,'Protector eterno','','','9',110,'11','11',1,1,3),(111,'Raíces del árbol sagrado','3, girar: Pon una ficha de aliado poder Barahai 3 FUE, 3 RES en juego.','Los barahai nacidos en este lugar sagrado, son bendecidos por la tierra en batalla.','2',111,'','',1,1,3),(112,'Rito de florecimiento','Agrega 3 energía. Recobrar 2.','\"¿Escuchas el sonido de su despertar?\" -Zustatek','2',112,'','',1,1,1),(113,'Runas de crecimiento ','El aliado objetivo obtiene +1 FUE por cada aliado que controles hasta el final del turno.','Su fuerza es colectiva.','1',113,'','',1,1,2),(114,'Sirena de voz salvaje','Girar: El aliado objetivo obtiene +3 FUE hasta el final del turno.','\"Dicen que su voz llena de coraje hasta los árboles dormidos.\" -Hermano chamán','4',114,'3','3',1,1,3),(115,'Tranquilidad','No se puede atacar a aliados este turno. Recobrar 3.','\"No permitiré que lastimes mis adoradas bestias.\" -Arasunu','3',115,'','',1,1,2),(116,'Vendaje eficiente','Remueve todo el daño del aliado objetivo. Toma una carta.','Los hermanos de la tierra han aprendido a tratar sus heridas tanto como afilar sus espadas.','2',116,'','',1,1,2),(117,'Alterar el destino','Busca en tu mazo una carta con vínculo y muéstrala. Luego mezcla tu mazo y pon esa carta en la parte superior. Recobrar 1.','Cuando aprendes a controlar el futuro, nada puede sorprenderte.','1',117,'','',1,5,2),(118,'Amaru','','','7',118,'7','7',1,5,1),(119,'Aniquilar','Destruye la carta objetivo.','Perplejos los caminantes observaron, cómo todo lo que conocían se reducía a escombros.','5',119,'','',1,5,3),(120,'Arrasar las ruinas','Exilia hasta tres cartas objetivo de un Inferno.','\"Esta civilización nunca existió.\" -At Fjerne','1',120,'','',1,5,1),(121,'Báculo relámpago','Girar: Tu demonio hace 1 daño al demonio objetivo.','Solo el báculo se escucha entre las ruinas de la antigua citadela.','2',121,'','',1,5,2),(122,'Bestia cola de luz','Protector.','La luz que irradia su cola atrae a las presas curiosas.','3',122,'2','2',1,5,1),(123,'Bosque de los primeros hijos','Los aliados obtienen +1 FUE cuando atacan hasta el final del turno.','La magia del bosque se siente en cada hoja, en cada insecto, en cada brisa.','0',123,'','',1,5,3),(124,'Botiquín médico','Destruir Botiquín médico: Remueve todo el daño del aliado objetivo.','\"Toma el mío, lo necesitarás mas que yo.\" -Últimas palabras del guía','1',124,'','',1,5,1),(125,'Buscadores de horizontes','Cuando Buscadores de horizontes entre en juego, busca en tu mazo un escenario, muéstralo, y ponlo en tu mano. Luego mezcla tu mazo.','\"¿Ya llegamos?\"','5',125,'4','4',1,5,3),(126,'Cadenas del campeón','El aliado equipado obtiene protector (Puedes girar este aliado y hacer que defienda al atacante, en vez del defensor propuesto).','','2',126,'0','3',1,5,2),(127,'Caminante valle moribundo','Vínculo 0: No se pueden jugar habilidades activadas hasta el final del turno.','','2',127,'1','3',1,5,2),(128,'Carbonizar','Tu demonio hace 3 daño al aliado objetivo.','\"Adoro el olor de carne quemada.\" -Akayra','2',128,'','',1,5,1),(129,'Centinela fronterizo','Protector.2, destruir Centinela de la frontera: El aliado objetivo obtiene Protector hasta el final del turno.','\"Defender a cualquier costo.\"','2',129,'1','3',1,5,1),(130,'Cienojos','Protector.','\"¿Es posible ver un Cienojos antes de que él te vea?\" Antiguo refrán','6',130,'3','7',1,5,2),(131,'Cofre del olvido','Cofre del olvido no se endereza en tu Restauración. Girar: Exilia la primer carta de un Inferno. Exiliar una carta de tu Inferno: Endereza Cofre del olvido.','Su creador se perdió para siempre, al igual que la última criatura que se atrevió a usarlo.','2',131,'','',1,5,2),(132,'Coleccionacráneos','','','5',132,'5','5',1,5,1),(133,'Cordillera de fuego','Siempre que se gire un recurso en el turno de un enemigo, Cordillera de fuego hace 1 daño al controlador de ese recurso.','Cada paso que des, puede ser tu útlimo en este mundo.','0',133,'','',1,5,3),(134,'Daga del silencio','El aliado equipado obtiene evasivo.','','2',134,'1','0',1,5,2),(135,'Deformidad sin nombre','Protector. Deformidad sin nombre obtiene +1 FUE por cada aliado que controles y +1 RES por cada aliado en tu Inferno.','','9',135,'3','5',1,5,4),(136,'Desgarrar la realidad','Mezcla las primeras siete cartas de tu Inferno en tu mazo. Luego exilia Desgarrar la realidad.','\"Mi conexión con tu mundo es más grande de lo que puedas siquiera conocer.\" -Viden','4',136,'','',1,5,2),(137,'Desintegración absoluta','Exilia todos los recursos.','','7',137,'','',1,5,4),(138,'Desvanecerse','Pon la carta objetivo en la mano de su demonio.','Hay ideas que no le agradan a nadie.','2',138,'','',1,5,1),(139,'Dragón infernal','Cuando Dragón infernal entre en juego, exilia tu Inferno.','Arrasa con todo ser vivo o muerto que atraviese su camino.','9',139,'12','9',1,5,3),(140,'Elemental luminoso','Los rituales que hagan objetivo a Elemental luminoso se pueden jugar sin pagar su coste.','Su sangre es energía líquida.','5',140,'5','5',1,5,3),(141,'Esfera de cristal','Todos los aliados entran en juego girados.','Es imposible entrar o salir de la ciudad sin su permiso. Es imposible comerciar, transitar y respirar.','0',141,'','',1,5,2),(142,'Espada absorbealmas','Cuando el aliado equipado destruya un aliado, puedes ponerlo en juego bajo tu control desde el inferno de su demonio.','La sola idea de regresar al Inferno, es suficiente para ofrecer tu eterna lealtad al portador.','5',142,'2','0',1,5,4),(143,'Estanque de rocío','Al final del turno de cada demonio, ese demonio remueve 1 daño de cada aliado que controle.','Los Hermanos de la tierra protegen el estanque con su vida, ya que el estanque siempre se las devuelve.','0',143,'','',1,5,2),(144,'Evaluar posibilidades','Pon X recursos que controles en tu mano.','\"Cambio mi riqueza por nuevo conocimiento.\"','X',144,'','',1,5,2),(145,'Forastero barahai','Agresor.','','3',145,'1','4',1,5,1),(146,'Forastero cyborg','Halo. Protector.','','4',146,'3','2',1,5,1),(147,'Forastero diablo','Evasivo.','','3',147,'2','2',1,5,1),(148,'Forastero kirch','Furia.','','3',148,'2','2',1,5,1),(149,'Garras de titanio','El aliado equipado obtiene halo.','','2',149,'1','0',1,5,2),(150,'Generador de luminium','Generador de luminium entra en juego girado. Girar: Agrega 1 energía.','\"Antiguo pero creativo diseño.\" -Bibliotecario de éter','2',150,'','',1,5,2),(151,'Gothika','Si una carta fuera a ir a un Inferno desde el juego, en vez de eso, exíliala.','En Gothika hay una sola regla que debes conocer a la perfección: ¡OBEDECE!','0',151,'','',1,5,3),(152,'Hachas arrojadizas','El aliado equipado obtiene furia.','','2',152,'1','0',1,5,2),(153,'Hija del valor','Hija del valor no puede atacar a aliados.','Pelear por una causa noble no siempre es suficiente.','1',153,'2','1',1,5,2),(154,'Homúnculo de laboratorio','X, destruir Homúnculo de laboratorio: Remueve X daño del aliado objetivo.','\"Oh señor mío, ¿nueva tarea tendrá usted?\"','2',154,'1','2',1,5,1),(155,'Horno cenizo','Al final del turno de cada demonio, si no controla aliados, Horno cenizo le hace 1 daño.','Si los Nungnarh dejaran sus tareas un solo día, su ciudad colapsaría.','0',155,'','',1,5,2),(156,'Ingeniero médico','Girar: Remueve 2 daño del aliado objetivo.','En estos tiempos un médico simplemente no es suficiente.','3',156,'1','4',1,5,1),(157,'Inyector de adrenalina','3, girar: Endereza el aliado objetivo','Un guerrero exhausto es un guerrero muerto.','3',157,'','',1,5,2),(158,'Lagarto escamacristal','Destruir Lagarto escamacristal: Agrega 1 energía.','\"Su carne es extremadamente sabrosa, aunque no es por eso que los buscan.\"','1',158,'1','1',1,5,1),(159,'Lago de almas perdidas','Cuando un aliado deje el juego, su controlador exilia las primeras dos cartas de su mazo.','\"Cuando mueres en esta tierra, tu alma sufre más dolor que tu cuerpo.\" Leyenda antigua','0',159,'','',1,5,2),(160,'Lanza del héroe','El aliado equipado obtiene agresor.','','2',160,'1','0',1,5,2),(161,'Manada de lobos','','','3',161,'3','3',1,5,1),(162,'Nueva tierra, nuevas reglas','Busca en tu mazo un escenario y ponlo en juego. Luego mezcla tu mazo.','\"Esta es MI TIERRA, y estas son TUS REGLAS.\" -Comandante nungnarh','3',162,'','',1,5,2),(163,'Orbe tribal','El aliado equipado obtiene +1 FUE por cada otro aliado de su tipo que controles.','','4',163,'0','0',1,5,3),(164,'Palacio escondido','Si la primer carta de un Inferno es ritual, obtiene Recobrar X, donde X es su coste.','El palacio esconde los más oscuros secretos.','0',164,'','',1,5,3),(165,'Pantano de los chamanes','Al final del combate, Pantano de los chamanes hace 1 daño al aliado atacante.','Cada nuevo cadáver, expande sus viscosas aguas.','0',165,'','',1,5,2),(166,'Patrulla experta','Patrulla experta obtiene +2 FUE y +2 RES por cada arma equipada.','Maestros del arte de guerra.','5',166,'4','4',1,5,2),(167,'Pócima de juventud temporal','En tu Restauración, puedes destruir Pócima de juventud temporal. Si lo haces, tienes un turno adicional después de éste.','','8',167,'','',1,5,3),(168,'Quebrar','Destruye el objeto o arma objetivo.','\"Ya no necesitarás esto.\" -Zustatek','2',168,'','',1,5,1),(169,'Quema de conocimiento','Busca en tu mazo hasta 3 rituales con recobrar y ponlos en tu Inferno. Luego mezcla tu mazo. Vínculo 0: Busca en tu mazo un ritual con recobrar y ponlo en tu Inferno. Luego mezcla tu mazo.','','2',169,'','',1,5,2),(170,'Recolector de cadáveres','Recolector de cadáveres no puede atacar a demonios.','Nadie se mete en sus dominios, nadie que esté vivo al menos…','4',170,'6','3',1,5,2),(171,'Réplica de cristal','Cuando otro arma entre en juego, destruye Réplica de cristal.','','3',171,'3','3',1,5,3),(172,'Resplandor','Previene todo el daño que se le fuera a hacer al aliado objetivo hasta el final del turno. Toma una carta.','','2',172,'','',1,5,1),(173,'Restablecer','Pon la carta objetivo de tu Inferno en tu mano. Recobrar 3.','En el bosque de los primeros hijos ningún árbol muere realmente.','3',173,'','',1,5,1),(174,'Sirena de voz tranquila','Previene todo el daño que le fueran a hacer los aliados.','Su voz y su belleza, encantan a cualquier ser viviente.','6',174,'6','2',1,5,3),(175,'Tomo de sabiduría inagotable','Cuando Tomo de sabiduría inagotable entre en juego, toma una carta. No tienes límite de cartas en tu mano.','En la biblioteca del éter el conocimiento crece como la vegetación silvestre.','1',175,'','',1,5,2),(176,'Valle moribundo','No se pueden jugar habilidades activadas.','Sin vida, sin conocimiento, sin futuro.','0',176,'','',1,5,2),(177,'Vampiro paciente','Si tus enemigos no tienen recursos enderezados, Vampiro paciente obtiene +2 FUE y +2 RES.','\"Mi oportunidad esta cerca, no puedes vigilarme eternamente…\"','3',177,'2','2',1,5,2),(178,'Vampiro pasomarchito','El escenario pierde todas sus habilidades. Vínculo 0: Destruye el escenario objetivo.','Marchita la tierra tan rápido como se expanden en ella.','4',178,'3','3',1,5,3),(179,'Vigía de las ruinas','Protector. Al final de tu turno, endereza Vigía de las ruinas.','Siempre listo.','4',179,'2','4',1,5,1),(180,'Vórtice al abismo','Exilia el aliado atacante objetivo.','\"Despídete de todos los mundos.\" -Unwaj','3',180,'','',1,5,2),(181,'Akayra, fuego eterno','','','',1,'0','',2,4,2),(182,'Rasap, forjador de victorias','','','',2,'0','',2,4,2),(183,'Syv, señor del desierto','','','',3,'1','',2,4,2),(184,'Baggrund, especuladora frenética','','','',4,'0','',2,3,2),(185,'Mimire,coleccionapensamientos','','','',5,'1','',2,3,2),(186,'Viden, el traicionado','','','',6,'99','',2,3,2),(187,'At Fjerne, peregrino infernal','','','',7,'0','',2,2,2),(188,'Naamah, artifice del submundo','','','',8,'1','',2,2,2),(189,'Zulfur, patriarca de los caídos','','','',9,'0','',2,2,2),(190,'Arasunu, amo de las bestias','','','',10,'1','',2,1,2),(191,'Dämonin, la esclavizada','','','',11,'3','',2,1,2),(192,'Zustatek, guardián del bosque','','','',12,'0','',2,1,2),(193,'Buscador de hoja ardiente','','','3',13,'2','2',2,4,NULL),(194,'Cabo de entrenamiento','','','3',14,'2','2',2,4,NULL),(195,'Cachorros Kirch','','','2',15,'3','1',2,4,NULL),(196,'Calé, ruina de Jama','','','3',16,'','',2,4,3),(197,'Caminante de los silos','','','3',17,'2','3',2,4,2),(198,'Chispa infortunada','','','1',18,'','',2,4,NULL),(199,'Cimitarra de cristal','','','4',19,'\"+2\"','\"+2\"',2,4,3),(200,'Cráneo de hierro fundido','','','2',20,'\"+1\"','\"+1\"',2,4,2),(201,'Hoja de la ira','','','3',21,'\"+3\"','\"+0\"',2,4,NULL),(202,'Horno de hierro','','','3',22,'','',2,4,NULL),(203,'Incendiar','','','1',23,'','',2,4,2),(204,'Incitación de Ubanna','','','3',24,'','',2,4,2),(205,'Infantes de vanguardia','','','2',25,'2','2',2,4,NULL),(206,'Inyección sangrearena','','','1',26,'','',2,4,NULL),(207,'Kirch berserker','','','3',27,'2','2',2,4,NULL),(208,'Kirch fuegodemencia','','','3',28,'2','2',2,4,NULL),(209,'Kirch persistente','','','2',29,'2','1',2,4,2),(210,'Kirch poseido','','','1',30,'1','1',2,4,2),(211,'Kirch solitario','','','0',31,'1','1',2,4,NULL),(212,'Lágrimas de Syv','','','7',32,'','',2,4,NULL),(213,'Luminiscencia demencial','','','1',33,'','',2,4,2),(214,'Maldición de sangre','','','4',34,'','',2,4,NULL),(215,'Manual de demolición','','','1',35,'','',2,4,3),(216,'Martillo de terremotos','','','3',36,'\"+2\"','\"+0\"',2,4,2),(217,'Máscara opresora','','','2',37,'','',2,4,NULL),(218,'Pandilla bruta','','','4',38,'4','2',2,4,NULL),(219,'Pesadilla del desierto','','','8',39,'8','4',2,4,3),(220,'Plataformas de combate','','','',40,'','',2,4,3),(221,'Portadora de la rabia','','','6',41,'4','4',2,4,3),(222,'Pozo de lava','','','2',42,'','',2,4,2),(223,'Seguidor de Tanger','','','5',43,'2','4',2,4,3),(224,'Sueño de Volker','','','1',44,'','',2,4,3),(225,'Tornado destructivo','','','1',45,'','',2,4,3),(226,'Volker, hacedor de la arena','','','6',46,'2','5',2,4,NULL),(227,'Acorazado de cobre','','','4',47,'3','3',2,3,NULL),(228,'Actualizar información','','','1',48,'','',2,3,3),(229,'Bomba de oscuridad','','','2',49,'','',2,3,NULL),(230,'Caminante solitario','','','3',50,'2','3',2,3,2),(231,'Campeón Cyborg','','','3',51,'2','4',2,3,NULL),(232,'Conocimiento acelerado','','','2',52,'','',2,3,NULL),(233,'Cubrir el acceso','','','4',53,'','',2,3,3),(234,'Cyborg paralizador','','','2',54,'1','3',2,3,NULL),(235,'Cyborg poseido','','','1',55,'1','1',2,3,2),(236,'Defensor melee','','','1',56,'1','1',2,3,NULL),(237,'Delos, runas del pasado','','','3',57,'','',2,3,3),(238,'Desintegrar','','','3',58,'','',2,3,3),(239,'Destilas ideas','','','2',59,'','',2,3,2),(240,'Domo de energia','','','3',60,'','',2,3,NULL),(241,'Droide explorador','','','2',61,'','',2,3,2),(242,'Efe, pionero de la rebelión','','','5',62,'3','4',2,3,NULL),(243,'Hoja de la pereza','','','3',63,'\"-3\"','\"-0\"',2,3,NULL),(244,'Inmovilidad demencial','','','2',64,'','',2,3,NULL),(245,'Jemo, la ciudad flotante','','','',65,'','',2,3,3),(246,'Luminium líquido','','','3',66,'','',2,3,3),(247,'Minotanke','','','5',67,'5','3',2,3,2),(248,'Nano revólver de implosión','','','4',68,'\"+2\"','\"+2\"',2,3,3),(249,'Ojo de la emboscada','','','1',69,'','',2,3,NULL),(250,'Persuadir','','','3',70,'','',2,3,2),(251,'Portadora de la sabiduría','','','6',71,'5','5',2,3,3),(252,'Predicción oportuna','','','1',72,'','',2,3,NULL),(253,'Reflector del panico','','','3',73,'','',2,3,2),(254,'Robar la cordura','','','4',74,'','',2,3,NULL),(255,'Sabios del vacío','','','4',75,'3','3',2,3,2),(256,'Telépata de la esfera','','','2',76,'2','2',2,3,NULL),(257,'Terapia láser','','','2',77,'','',2,3,2),(258,'Tratante de conocimiento','','','4',78,'3','4',2,3,3),(259,'Visión profunda','','','1',79,'','',2,3,NULL),(260,'Yelmo de la verdad','','','1',80,'','',2,3,NULL),(261,'Altar de At Fjerne','','','3',81,'','',2,2,NULL),(262,'Alterar el cuerpo','','','1',82,'','',2,2,NULL),(263,'Antigua abadía','','','',83,'','',2,2,3),(264,'Apertura infernal','','','1',84,'','',2,2,NULL),(265,'Calavera grabada','','','2',85,'','',2,2,2),(266,'Caminante de la abadía','','','3',86,'2','3',2,2,2),(267,'Campeón diablo','','','3',87,'4','2',2,2,NULL),(268,'Cántico sorginak','','','3',88,'3','2',2,2,NULL),(269,'Cruz repelente','','','1',89,'','',2,2,NULL),(270,'Démona, espina del tormento','','','3',90,'','',2,2,3),(271,'Diablo acorazado','','','6',91,'5','3',2,2,2),(272,'Diablo camaleón','','','2',92,'1','3',2,2,NULL),(273,'Diablo ofuscado','','','3',93,'3','3',2,2,NULL),(274,'Diablo poseido','','','1',94,'1','1',2,2,2),(275,'Diablo precavido','','','2',95,'2','2',2,2,NULL),(276,'Disgregar almas','','','3',96,'','',2,2,2),(277,'Entregar a la demencia','','','3',97,'','',2,2,2),(278,'Exposición solar','','','2',98,'','',2,2,2),(279,'Hoja de la envidia','','','3',99,'\"-1\"','\"+2\"',2,2,NULL),(280,'Hoja de Sekhmet','','','4',100,'\"+2\"','\"+2\"',2,2,3),(281,'Ira de Naamah','','','5',101,'','',2,2,3),(282,'Mesón de operaciones','','','5',102,'','',2,2,3),(283,'Nigromante diablo','','','4',103,'2','3',2,2,3),(284,'Portadora de la desgracia','','','6',104,'4','3',2,2,3),(285,'Profecía material','','','1',105,'','',2,2,3),(286,'Pua exterminadora','','','1',106,'','',2,2,NULL),(287,'Rastro de niebla','','','3',107,'','',2,2,NULL),(288,'Reanimar los caídos','','','4',108,'','',2,2,2),(289,'Reclutadora sorginak','','','2',109,'2','1',2,2,NULL),(290,'Robo vital','','','3',110,'','',2,2,NULL),(291,'Sanguijuela explosiva','','','4',111,'2','3',2,2,NULL),(292,'Sorginak lider','','','4',112,'4','3',2,2,3),(293,'Sorginaks del norte','','','4',113,'4','2',2,2,2),(294,'Vurdalak, Ejecutor de Venganza','','','4',114,'2','3',2,2,NULL),(295,'Araña gigante','','','8',115,'9','7',2,1,2),(296,'Baku, señor de los reptiles','','','4',116,'2','2',2,1,NULL),(297,'Barahai agerrido','','','1',117,'1','2',2,1,NULL),(298,'Barahai macizo','','','5',118,'4','3',2,1,NULL),(299,'Barahai poseido','','','1',119,'1','1',2,1,2),(300,'Barahai protegido','','','5',120,'5','5',2,1,2),(301,'Barahai rabiademencia','','','4',121,'3','1',2,1,NULL),(302,'Barahai vanidoso','','','6',122,'4','4',2,1,2),(303,'Brujula de Estari','','','1',123,'','',2,1,3),(304,'Caminante de Isgar','','','3',124,'2','3',2,1,2),(305,'Campeon barahai','','','2',125,'2','2',2,1,NULL),(306,'Comandante de Trion','','','4',126,'2','4',2,1,2),(307,'Concilio Púrpura','','','1',127,'','',2,1,3),(308,'Cortes de rabia','','','1',128,'','',2,1,NULL),(309,'Crecimiento demencial','','','1',129,'','',2,1,NULL),(310,'Daga despiedada','','','3',130,'','',2,1,2),(311,'Dragón felino','','','6',131,'7','7',2,1,NULL),(312,'Espejo de Columbus','','','1',132,'','',2,1,NULL),(313,'Grito de batalla','','','8',133,'','',2,1,NULL),(314,'Guardián de Tresia','','','2',134,'','',2,1,NULL),(315,'Guerrero rhino','','','4',135,'4','2',2,1,NULL),(316,'Hoja de la gula','','','3',136,'\"+2\"','\"+1\"',2,1,NULL),(317,'Pacificadora barahai','','','2',137,'1','1',2,1,NULL),(318,'Poción preventiva','','','2',138,'','',2,1,NULL),(319,'Portadora de la abundancia','','','6',139,'5','5',2,1,3),(320,'Raices profundas','','','2',140,'','',2,1,2),(321,'Revitalizar','','','4',141,'','',2,1,3),(322,'Sanadora de pétalos','','','3',142,'2','2',2,1,NULL),(323,'Sauro','','','9',143,'9','9',2,1,3),(324,'Sífile, trance sobrenatural','','','1',144,'','',2,1,3),(325,'Téjere, fuego de la tempestad','','','3',145,'\"+3\"','\"+3\"',2,1,3),(326,'Toque de Dämonin','','','2',146,'','',2,1,2),(327,'Trion, la caída','','','',147,'','',2,1,3),(328,'Zahane, hija del destino','','','1',148,'2','2',2,1,3),(329,'Acólito de Ubanna','','','3',149,'2','3',2,5,NULL),(330,'Beso vampírico','','','3',150,'','',2,5,NULL),(331,'Caminante demencial','','','3',151,'2','3',2,5,NULL),(332,'Campeón vampiro','','','4',152,'X','X',2,5,3),(333,'Cavernas de Insania','','','',153,'','',2,5,2),(334,'Clasificación Precisa','','','2',154,'','',2,5,3),(335,'Colmillo maldito','','','0',155,'','',2,5,NULL),(336,'Cubo demencial','','','3',156,'','',2,5,2),(337,'Daga sanguínea','','','3',157,'\"+1\"','\"+1\"',2,5,NULL),(338,'Forajido Vampiro','','','2',158,'1','1',2,5,2),(339,'Grilletes del sufrimiento','','','2',159,'','',2,5,NULL),(340,'Krilatis migratorios','','','1',160,'1','1',2,5,NULL),(341,'Llanura de perdición','','','',161,'','',2,5,2),(342,'Mandíbula de dragón','','','3',162,'\"+2\"','\"+2\"',2,5,2),(343,'Manto de las mil almas','','','3',163,'','',2,5,NULL),(344,'Marca de Caín','','','1',164,'','',2,5,NULL),(345,'Mike, el irrespetuoso','','','4',165,'2','5',2,5,NULL),(346,'Necrópolis olvidada','','','',166,'','',2,5,NULL),(347,'Negar la demencia','','','2',167,'','',2,5,3),(348,'Planus, llave del mundo esencial','','','2',168,'','',2,5,3),(349,'Regenerar con sangre','','','2',169,'','',2,5,2),(350,'Sangreatero','','','4',170,'3','3',2,5,2),(351,'Sol ennegrecido','','','0',171,'','',2,5,2),(352,'Vampiro cabecilla','','','6',172,'4','4',2,5,2),(353,'Vampiro Líder','','','6',173,'5','5',2,5,3),(354,'Vampiro persuasivo','','','3',174,'3','3',2,5,2),(355,'Vampiro recluta','','','3',175,'1','3',2,5,2),(356,'Vampiro sabio','','','3',176,'2','3',2,5,3),(357,'Vampiro sanador','','','4',177,'2','4',2,5,2),(358,'Vampiro temprano','','','2',178,'2','1',2,5,NULL),(359,'Viajero nocturno','','','1',179,'1','1',2,5,NULL),(360,'Vigía de de las tinieblas','','','4',180,'3','2',2,5,NULL);
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_artist`
--

DROP TABLE IF EXISTS `cards_artist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_artist` (
  `ARTIST_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`ARTIST_ID`,`IDX`),
  KEY `FK6EB7CBC3128518F3` (`ARTIST_ID`),
  KEY `FK6EB7CBC3F6A698C4` (`elt`),
  CONSTRAINT `FK6EB7CBC3F6A698C4` FOREIGN KEY (`elt`) REFERENCES `artists` (`ARTIST_ID`),
  CONSTRAINT `FK6EB7CBC3128518F3` FOREIGN KEY (`ARTIST_ID`) REFERENCES `cards` (`CARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_artist`
--

LOCK TABLES `cards_artist` WRITE;
/*!40000 ALTER TABLE `cards_artist` DISABLE KEYS */;
INSERT INTO `cards_artist` VALUES (14,4,0),(23,4,0),(25,4,0),(42,4,0),(52,4,0),(55,4,0),(58,4,0),(61,4,0),(82,4,0),(99,4,0),(102,4,0),(106,4,0),(41,5,0),(45,5,0),(69,5,0),(97,5,0),(156,5,0),(33,14,0),(73,14,0),(129,14,0),(145,14,0),(147,14,0),(65,15,0),(81,15,0),(117,15,0),(3,16,1),(16,16,1),(20,16,1),(28,16,1),(43,16,1),(70,16,1),(72,16,1),(79,16,1),(90,16,1),(98,16,1),(108,16,1),(113,16,1),(142,16,1),(163,16,1),(170,27,1),(32,28,0),(116,28,0),(38,34,0),(39,34,0),(170,37,0),(125,40,0),(14,41,1),(21,41,1),(23,41,1),(26,41,1),(30,41,1),(42,41,1),(51,41,1),(55,41,1),(58,41,1),(67,41,1),(75,41,1),(76,41,1),(80,41,1),(82,41,1),(83,41,1),(96,41,1),(99,41,1),(101,41,0),(102,41,1),(104,41,1),(126,41,1),(131,41,1),(149,41,1),(152,41,1),(157,41,1),(166,41,1),(171,41,1),(175,41,1),(13,42,0),(46,42,0),(105,42,0),(120,42,0),(24,43,0),(76,43,0),(90,43,0),(96,43,0),(166,43,0),(141,45,0),(151,45,0),(155,45,0),(165,45,0);
/*!40000 ALTER TABLE `cards_artist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_subtype`
--

DROP TABLE IF EXISTS `cards_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_subtype` (
  `SUBTYPE_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`SUBTYPE_ID`,`IDX`),
  KEY `FK2497D07E1E539E0` (`SUBTYPE_ID`),
  KEY `FK2497D07E9A549F57` (`elt`),
  CONSTRAINT `FK2497D07E9A549F57` FOREIGN KEY (`elt`) REFERENCES `subtypes` (`SUBTYPE_ID`),
  CONSTRAINT `FK2497D07E1E539E0` FOREIGN KEY (`SUBTYPE_ID`) REFERENCES `cards` (`CARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_subtype`
--

LOCK TABLES `cards_subtype` WRITE;
/*!40000 ALTER TABLE `cards_subtype` DISABLE KEYS */;
INSERT INTO `cards_subtype` VALUES (118,1,0),(130,1,0),(295,2,0),(329,3,0),(92,4,0),(93,4,0),(94,4,0),(95,4,0),(96,4,0),(100,4,0),(145,4,0),(297,4,0),(298,4,0),(299,4,0),(300,4,0),(301,4,0),(302,4,0),(305,4,0),(317,4,0),(110,5,0),(122,5,0),(311,5,0),(40,6,0),(47,6,0),(48,6,0),(49,6,0),(50,6,0),(61,6,0),(146,6,0),(231,6,0),(234,6,0),(235,6,0),(236,6,0),(242,6,0),(247,6,0),(258,6,0),(73,7,0),(74,7,0),(75,7,0),(76,7,0),(77,7,0),(81,7,0),(147,7,0),(267,7,0),(271,7,0),(272,7,0),(273,7,0),(274,7,0),(275,7,0),(283,7,0),(26,8,0),(109,8,0),(139,8,0),(140,8,0),(20,9,0),(51,9,0),(72,9,0),(104,9,0),(135,9,0),(219,9,0),(15,11,0),(16,11,0),(18,11,0),(19,11,0),(22,11,0),(23,11,0),(25,11,0),(42,11,0),(43,11,0),(45,11,0),(63,11,0),(64,11,0),(65,11,0),(68,11,0),(69,11,0),(70,11,0),(82,11,0),(97,11,0),(98,11,0),(99,11,0),(107,11,0),(108,11,0),(125,11,0),(127,11,0),(129,11,0),(153,11,0),(156,11,0),(166,11,0),(179,11,0),(193,11,0),(194,11,0),(197,11,0),(205,11,0),(218,11,0),(223,11,0),(230,11,0),(255,11,0),(256,11,0),(266,11,0),(268,11,0),(289,11,0),(292,11,0),(293,11,0),(304,11,0),(306,11,0),(315,11,0),(322,11,0),(328,11,0),(291,12,0),(17,13,0),(30,13,0),(31,13,0),(32,13,0),(33,13,0),(148,13,0),(195,13,0),(207,13,0),(208,13,0),(209,13,0),(210,13,0),(211,13,0),(161,14,0),(132,15,0),(170,15,0),(158,16,0),(296,16,0),(323,16,0),(340,16,0),(227,18,0),(177,19,0),(178,19,0),(226,19,0),(242,19,1),(294,19,0),(296,19,1),(331,19,0),(332,19,0),(338,19,0),(345,19,0),(350,19,0),(352,19,0),(353,19,0),(354,19,0),(355,19,0),(356,19,0),(357,19,0),(358,19,0),(359,19,0),(360,19,0);
/*!40000 ALTER TABLE `cards_subtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_supertype`
--

DROP TABLE IF EXISTS `cards_supertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_supertype` (
  `SUPERTYPE_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`SUPERTYPE_ID`,`IDX`),
  KEY `FK2883BB391C87AF12` (`elt`),
  KEY `FK2883BB39E027C045` (`SUPERTYPE_ID`),
  CONSTRAINT `FK2883BB39E027C045` FOREIGN KEY (`SUPERTYPE_ID`) REFERENCES `cards` (`CARD_ID`),
  CONSTRAINT `FK2883BB391C87AF12` FOREIGN KEY (`elt`) REFERENCES `supertypes` (`SUPERTYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_supertype`
--

LOCK TABLES `cards_supertype` WRITE;
/*!40000 ALTER TABLE `cards_supertype` DISABLE KEYS */;
INSERT INTO `cards_supertype` VALUES (340,1,0),(20,3,0),(35,3,0),(51,3,0),(60,3,0),(72,3,0),(89,3,0),(104,3,0),(114,3,0),(135,3,0),(174,3,0),(196,3,0),(221,3,0),(226,3,0),(237,3,0),(242,3,0),(270,3,0),(284,3,0),(294,3,0),(296,3,0),(314,3,0),(319,3,0),(324,3,0),(328,3,0),(348,3,0),(1,4,0),(2,4,0),(3,4,0),(4,4,0),(5,4,0),(6,4,0),(7,4,0),(8,4,0),(9,4,0),(10,4,0),(11,4,0),(12,4,0),(181,4,0),(182,4,0),(183,4,0),(184,4,0),(185,4,0),(186,4,0),(187,4,0),(188,4,0),(189,4,0),(190,4,0),(191,4,0),(192,4,0);
/*!40000 ALTER TABLE `cards_supertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_type`
--

DROP TABLE IF EXISTS `cards_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_type` (
  `TYPE_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`TYPE_ID`,`IDX`),
  KEY `FK7AF46AB6F0503CB7` (`elt`),
  KEY `FK7AF46AB6BD6D8020` (`TYPE_ID`),
  CONSTRAINT `FK7AF46AB6BD6D8020` FOREIGN KEY (`TYPE_ID`) REFERENCES `cards` (`CARD_ID`),
  CONSTRAINT `FK7AF46AB6F0503CB7` FOREIGN KEY (`elt`) REFERENCES `types` (`TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_type`
--

LOCK TABLES `cards_type` WRITE;
/*!40000 ALTER TABLE `cards_type` DISABLE KEYS */;
INSERT INTO `cards_type` VALUES (15,1,0),(16,1,0),(17,1,0),(18,1,0),(19,1,0),(20,1,0),(22,1,0),(23,1,0),(25,1,0),(26,1,0),(30,1,0),(31,1,0),(32,1,0),(33,1,0),(35,1,0),(40,1,0),(42,1,0),(43,1,0),(45,1,0),(47,1,0),(48,1,0),(49,1,0),(50,1,0),(51,1,0),(60,1,0),(61,1,0),(63,1,0),(64,1,0),(65,1,0),(68,1,0),(69,1,0),(70,1,0),(72,1,0),(73,1,0),(74,1,0),(75,1,0),(76,1,0),(77,1,0),(81,1,0),(82,1,0),(89,1,0),(92,1,0),(93,1,0),(94,1,0),(95,1,0),(96,1,0),(97,1,0),(98,1,0),(99,1,0),(100,1,0),(104,1,0),(107,1,0),(108,1,0),(109,1,0),(110,1,0),(114,1,0),(118,1,0),(122,1,0),(125,1,0),(127,1,0),(129,1,0),(130,1,0),(132,1,0),(135,1,0),(139,1,0),(140,1,0),(145,1,0),(146,1,0),(147,1,0),(148,1,0),(153,1,0),(154,1,0),(156,1,0),(158,1,0),(161,1,0),(166,1,0),(170,1,0),(174,1,0),(177,1,0),(178,1,0),(179,1,0),(193,1,0),(194,1,0),(195,1,0),(197,1,0),(205,1,0),(207,1,0),(208,1,0),(209,1,0),(210,1,0),(211,1,0),(218,1,0),(219,1,0),(221,1,0),(223,1,0),(226,1,0),(227,1,0),(230,1,0),(231,1,0),(234,1,0),(235,1,0),(236,1,0),(242,1,0),(247,1,0),(251,1,0),(255,1,0),(256,1,0),(258,1,0),(266,1,0),(267,1,0),(268,1,0),(271,1,0),(272,1,0),(273,1,0),(274,1,0),(275,1,0),(283,1,0),(284,1,0),(289,1,0),(291,1,0),(292,1,0),(293,1,0),(294,1,0),(295,1,0),(296,1,0),(297,1,0),(298,1,0),(299,1,0),(300,1,0),(301,1,0),(302,1,0),(304,1,0),(305,1,0),(306,1,0),(311,1,0),(315,1,0),(317,1,0),(319,1,0),(322,1,0),(323,1,0),(328,1,0),(329,1,0),(331,1,0),(332,1,0),(338,1,0),(340,1,0),(345,1,0),(350,1,0),(352,1,0),(353,1,0),(354,1,0),(355,1,0),(356,1,0),(357,1,0),(358,1,0),(359,1,0),(360,1,0),(27,2,0),(57,2,0),(71,2,0),(103,2,0),(126,2,0),(134,2,0),(142,2,0),(149,2,0),(152,2,0),(160,2,0),(163,2,0),(171,2,0),(199,2,0),(200,2,0),(201,2,0),(216,2,0),(243,2,0),(248,2,0),(279,2,0),(280,2,0),(316,2,0),(325,2,0),(337,2,0),(342,2,0),(13,3,0),(14,3,0),(21,3,0),(24,3,0),(28,3,0),(34,3,0),(36,3,0),(37,3,0),(39,3,0),(41,3,0),(44,3,0),(46,3,0),(52,3,0),(53,3,0),(55,3,0),(56,3,0),(59,3,0),(62,3,0),(66,3,0),(78,3,0),(79,3,0),(80,3,0),(84,3,0),(85,3,0),(86,3,0),(87,3,0),(88,3,0),(90,3,0),(91,3,0),(101,3,0),(105,3,0),(106,3,0),(112,3,0),(113,3,0),(115,3,0),(116,3,0),(117,3,0),(119,3,0),(120,3,0),(128,3,0),(136,3,0),(137,3,0),(138,3,0),(144,3,0),(162,3,0),(168,3,0),(169,3,0),(172,3,0),(173,3,0),(180,3,0),(198,3,0),(203,3,0),(204,3,0),(212,3,0),(213,3,0),(214,3,0),(224,3,0),(225,3,0),(228,3,0),(232,3,0),(233,3,0),(238,3,0),(239,3,0),(244,3,0),(250,3,0),(252,3,0),(254,3,0),(257,3,0),(259,3,0),(262,3,0),(264,3,0),(276,3,0),(277,3,0),(278,3,0),(281,3,0),(285,3,0),(287,3,0),(288,3,0),(290,3,0),(307,3,0),(308,3,0),(309,3,0),(313,3,0),(321,3,0),(326,3,0),(330,3,0),(334,3,0),(344,3,0),(347,3,0),(349,3,0),(29,4,0),(38,4,0),(54,4,0),(58,4,0),(67,4,0),(83,4,0),(102,4,0),(111,4,0),(121,4,0),(124,4,0),(131,4,0),(150,4,0),(157,4,0),(167,4,0),(175,4,0),(196,4,0),(202,4,0),(206,4,0),(215,4,0),(217,4,0),(222,4,0),(240,4,0),(241,4,0),(246,4,0),(249,4,0),(253,4,0),(260,4,0),(261,4,0),(265,4,0),(269,4,0),(270,4,0),(282,4,0),(286,4,0),(303,4,0),(310,4,0),(312,4,0),(314,4,0),(318,4,0),(320,4,0),(324,4,0),(335,4,0),(336,4,0),(339,4,0),(343,4,0),(348,4,0),(351,4,0),(123,5,0),(133,5,0),(141,5,0),(143,5,0),(151,5,0),(155,5,0),(159,5,0),(164,5,0),(165,5,0),(176,5,0),(220,5,0),(245,5,0),(263,5,0),(327,5,0),(333,5,0),(341,5,0),(346,5,0);
/*!40000 ALTER TABLE `cards_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `decks`
--

DROP TABLE IF EXISTS `decks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decks` (
  `DECK_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `DEMON_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`DECK_ID`),
  UNIQUE KEY `name` (`name`),
  KEY `FK3DEA0CA236166CF` (`DEMON_ID`),
  CONSTRAINT `FK3DEA0CA236166CF` FOREIGN KEY (`DEMON_ID`) REFERENCES `cards` (`CARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decks`
--

LOCK TABLES `decks` WRITE;
/*!40000 ALTER TABLE `decks` DISABLE KEYS */;
/*!40000 ALTER TABLE `decks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `decks_cards`
--

DROP TABLE IF EXISTS `decks_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decks_cards` (
  `CARD_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`CARD_ID`,`IDX`),
  KEY `FK6FCEABAEF048288D` (`elt`),
  KEY `FK6FCEABAE114B7E23` (`CARD_ID`),
  CONSTRAINT `FK6FCEABAE114B7E23` FOREIGN KEY (`CARD_ID`) REFERENCES `decks` (`DECK_ID`),
  CONSTRAINT `FK6FCEABAEF048288D` FOREIGN KEY (`elt`) REFERENCES `cards` (`CARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decks_cards`
--

LOCK TABLES `decks_cards` WRITE;
/*!40000 ALTER TABLE `decks_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `decks_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expansions`
--

DROP TABLE IF EXISTS `expansions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expansions` (
  `EXPANSION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EXPANSION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expansions`
--

LOCK TABLES `expansions` WRITE;
/*!40000 ALTER TABLE `expansions` DISABLE KEYS */;
INSERT INTO `expansions` VALUES (1,'Despertar'),(2,'Insania');
/*!40000 ALTER TABLE `expansions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frequencies`
--

DROP TABLE IF EXISTS `frequencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frequencies` (
  `FREQUENCY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FREQUENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frequencies`
--

LOCK TABLES `frequencies` WRITE;
/*!40000 ALTER TABLE `frequencies` DISABLE KEYS */;
INSERT INTO `frequencies` VALUES (1,'Común','fcffff'),(2,'Infrecuente','e91b25'),(3,'Rara','fff300'),(4,'Épica','02aeee');
/*!40000 ALTER TABLE `frequencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pathways`
--

DROP TABLE IF EXISTS `pathways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pathways` (
  `PATHWAY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `primaryColor` varchar(255) DEFAULT NULL,
  `secondColor` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`PATHWAY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pathways`
--

LOCK TABLES `pathways` WRITE;
/*!40000 ALTER TABLE `pathways` DISABLE KEYS */;
INSERT INTO `pathways` VALUES (1,'Poder','007c00','004d00','En las húmedas y selváticas tierras del Poder, la presencia de los Demonios potenció el crecimiento de toda vegetación, sin conocer límite. Los humanos aprendieron a adaptarse construyendo sus viviendas alrededor y dentro de grandes árboles. Entrenaron a los animales salvajes que habitaban, y los prepararon para la guerra.'),(2,'Muerte','9f2fa9','790483','En las góticas ciudades de la Muerte, los humanos continúan con sus linajes medievales, compartiendo sus secretos y hechicería a su oscura progenie. Poseen un profundo vínculo con la vida y la muerte, realizando rituales en ofrenda a los Demonios. Tratar de conseguir su conocimiento puede traducirse en participar activamente de un ritual de sacrificio...'),(3,'Locura','425bd9','0c27b0','En las actualizadas ciudades de la Locura, el conocimiento y el tiempo es la moneda de cambio. Su ambición por el control total de la física y la tecnología ha llevado a estos humanos a consumir cuanto recurso encuentran, situación que los mantiene en constante conflicto con los demás seres terrenales. Gracias a sus avanzadas infraestructuras, se protegen del mundo exterior en sus ciudades flotantes. Pero aún asi, ¿pueden protegerse del INFERNO?'),(4,'Caos','ed693b','de4c18','En las áridas y calurosas tierras del Caos, los humanos han sobrevivido a los Demonios y desarrollado una gran fortaleza. Grandes herreros y maestros del armamento, los señores del Caos aprovechan la tecnología para hacer un sinnúmero de poderosas armas y un ejército de criaturas mutantes.'),(5,'Neutral','425bd9','0c27b0','');
/*!40000 ALTER TABLE `pathways` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subtypes`
--

DROP TABLE IF EXISTS `subtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subtypes` (
  `SUBTYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SUBTYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subtypes`
--

LOCK TABLES `subtypes` WRITE;
/*!40000 ALTER TABLE `subtypes` DISABLE KEYS */;
INSERT INTO `subtypes` VALUES (1,'Abominación'),(2,'Araña'),(3,'Avatar'),(4,'Barahai'),(5,'Bestia'),(6,'Cyborg'),(7,'Diablo'),(8,'Elemental'),(9,'Encarnación'),(10,'Homunculo'),(11,'Humano'),(12,'Insecto'),(13,'Kirch'),(14,'Lobo'),(15,'Mutante'),(16,'Reptil'),(17,'Sucubo'),(18,'Tanque'),(19,'Vampiro'),(20,'Bot');
/*!40000 ALTER TABLE `subtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supertypes`
--

DROP TABLE IF EXISTS `supertypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supertypes` (
  `SUPERTYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SUPERTYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supertypes`
--

LOCK TABLES `supertypes` WRITE;
/*!40000 ALTER TABLE `supertypes` DISABLE KEYS */;
INSERT INTO `supertypes` VALUES (1,'Ilimitado'),(2,'Instántaneo'),(3,'Único'),(4,'Demonio');
/*!40000 ALTER TABLE `supertypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `types` (
  `TYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (1,'Aliado'),(2,'Arma'),(3,'Ritual'),(4,'Objeto'),(5,'Escenario');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-10-13 19:52:30
