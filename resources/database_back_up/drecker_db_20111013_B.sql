-- MySQL dump 10.13  Distrib 5.1.58, for debian-linux-gnu (x86_64)
--
-- Host: fallen.dyndns-server.com    Database: drecker
-- ------------------------------------------------------
-- Server version	5.1.47-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `drecker`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `drecker` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `drecker`;

--
-- Table structure for table `artists`
--

DROP TABLE IF EXISTS `artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artists` (
  `ARTIST_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ARTIST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artists`
--

LOCK TABLES `artists` WRITE;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
INSERT INTO `artists` VALUES (1,'Adrian','Kim'),(2,'Adrian','Zotto'),(3,'Alberto','Aprea'),(4,'Alejandro','Machado'),(5,'Brian','Miroglio'),(6,'Damian','Graff'),(7,'Danilo','Garcia Medrano'),(8,'Dario','Reyes'),(9,'Edgar','Machiavello'),(10,'Emmanuel','Bou'),(11,'Esteban','CamposMillapell'),(12,'Fabian Alberto','Gonzales'),(13,'Fabian Mayat','Aguilar'),(14,'Felipe','Araya'),(15,'Fernando','Morales'),(16,'Franco','Riesco'),(17,'Gaston','Real'),(18,'Gaston','Zubeldia'),(19,'Julian','Nobile'),(20,'German','Nobile'),(21,'Guillermo','Villareal'),(22,'Gustavo','Santome'),(23,'Hans','Lobos'),(24,'Ignacio','Bustos'),(25,'Ivanna','Matilla'),(26,'Juan Manuel','Tumburus'),(27,'Juan Martinez','Pinilla'),(28,'Leonardo','Vallejos'),(29,'Lizzo',''),(30,'Luciano','Comizzo'),(31,'Luciano','Fleitas'),(32,'Manuela','Mauregui'),(33,'Matias','Montenegro'),(34,'MeKenzie','Martin'),(35,'Nicolas','Fernandez'),(36,'Pablo','Gorigoitia'),(37,'Paulo','Barrios'),(38,'Roberto','Viacava'),(39,'Rodolfo','Buscaglia'),(40,'Sandro','Rybak'),(41,'Santiago','Calleriza'),(42,'Sara','Villegas'),(43,'Sergio','Tarquini'),(44,'Stryke',''),(45,'Tim','Keil'),(46,'Tomas','Aira'),(47,'Veronica','Vallejos');
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
  `CARD_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `effect` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `str` varchar(255) DEFAULT NULL,
  `res` varchar(255) DEFAULT NULL,
  `EXPANSION_ID` bigint(20) DEFAULT NULL,
  `PATHWAY_ID` bigint(20) DEFAULT NULL,
  `FREQUENCY_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`CARD_ID`),
  KEY `FK3CEEF4348B7A2AA` (`PATHWAY_ID`),
  KEY `FK3CEEF43780F816A` (`FREQUENCY_ID`),
  KEY `FK3CEEF433363228A` (`EXPANSION_ID`),
  CONSTRAINT `FK3CEEF433363228A` FOREIGN KEY (`EXPANSION_ID`) REFERENCES `expansions` (`EXPANSION_ID`),
  CONSTRAINT `FK3CEEF4348B7A2AA` FOREIGN KEY (`PATHWAY_ID`) REFERENCES `pathways` (`PATHWAY_ID`),
  CONSTRAINT `FK3CEEF43780F816A` FOREIGN KEY (`FREQUENCY_ID`) REFERENCES `frequencies` (`FREQUENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (6,'Viden, el omnisciente','Cuando tu mazo tenga quince o menos cartas, voltea Viden.','\"Conozco el día que perecerás. Adivinaste.\"',NULL,6,'2',NULL,1,3,1),(15,'Buscador de violencia perdida','Vinculo 0: Pon el arma objetivo de coste 3 o menos de tu Inferno en tu mano.','Poseen un vinculo poderoso con el Inferno.','2',15,'2','1',1,4,1),(21,'Acorazado de cobre','Protector (Puedes girar este aliado y hacer que defienda al atacante, en vez del defensor propuesto).','La ciudad debe crecer, el bosque deberá buscar otro hogar.','4',47,'3','3',2,3,1),(22,'Actualizar información','Busca en tu mazo un ritual y muéstralo. Luego, mezcla tu mazo y pon esa carta en la parte superior.','\"Los implantes del tórax parecieron responder a alguna orden transmitida y abrieron un compartimiento circular en el abdómen.\" Fragmento de Efe, pionero de la rebelión.','1',48,'','',2,3,3),(23,'Bomba de oscuridad','Cuando bomba de oscuridad entre en juego, toma una carta.\nDestruir Bomba de oscuridad: El aliado objetivo pierde todas sus habilidades hasta el final del turno.','\"El artilugio los tomó por sorpresa, y no tuvieron tiempo de repelerlo.\" Fragmentos de Efe, pionero de la rebelión.','2',49,'','',2,3,1),(24,'Caminante solitario','Vínculo (1)- El demonio objetivo descarta una carta al azar.','\"¿Me harías una lectura? Sé que es un privilegio de los veneradores, ¡Pero puedo pagarte una cifra voluminosa!\" -Aspen, caminante solitario.','3',50,'2','3',2,3,2),(25,'Campeón cyborg','','','3',51,'2','4',2,3,1),(26,'Conocimiento acelerado','Toma dos cartas, luego mezcla una carta de tu mano en tu mazo.\nRecobrar (3).','\"Filtrando información, el cerebro humano es muy imperfecto.\" -Mímire','2',52,'','',2,3,1),(27,'Cubrir el acceso','Voltea el demonio objetivo si se encuentra en su forma terrenal.','\"Es un ritual sencillo. No lo he probado aún. Pero sospecho que con la ayuda de su maginficencia podría abrir un portal tan grande como para hacer pasar por él a todo Jemo.\" -Efe, pionero de la rebelión','4',53,'','',2,3,3),(28,'Cyborg paralizador','Cuando Cyborg paralizador entre en juego, si hay Thanatos, el aliado objetivo pierde todas sus habilidades hasta el final del turno.','','2',54,'1','3',2,3,1),(29,'Cyborg poseído','Cyborg poseído obtiene +1 FUE, +1 RES y tiene la habilidad de halo mientras haya Thanatos.\nCyborg poseído es un vampiro además de sus otros tipos mientras haya Thanatos.','Cuando la demencia se apodera de la locura, los resultados no son alentadores.','1',55,'1','1',2,3,2),(30,'Defensor melee','Halo (No puede ser objetivo de cartas o habilidades de un enemigo).','La segunda barrera de protección de Jemo.','1',56,'1','1',2,3,1),(31,'Delos, runa del pasado','Los demonios juegan mostrando la carta superior de su mazo.\nGirar: El demonio objetivo mezcla su mazo.','\"Las aguas del río pueden traer valiosos objetos si se aprende a manipular sus corrientes.\" -Mímire','3',57,'','',2,3,3),(32,'Desintegrar','Mezcla el aliado objetivo en el mazo de su demonio.','\"¿Crees que en algún lugar de la tierra estarías a salvo de la red de Viden?\" Fragmento de Efe, pionero de la rebelión','3',58,'','',2,3,3),(33,'Destilar ideas','Pon el recurso objetivo que controles en tu mano.\nRecobrar (2).','\"La materia es energía, al igual que el conocimiento.\" -Mímire','2',59,'','',2,3,2),(34,'Domo de energía','Siempre que un aliado enemigo ataque a tu demonio, su controlador puede pagar (2). Si no lo hace, cancela el combate.','\"El domo de defensa estaba encendido, cubriendo la ciudad con una capa de energía que hacía imposible su entrada.\" Fragmento de Efe, pionero de la rebelión','3',60,'','',2,3,4),(35,'Droide explorador','Destruir Droide explorador: Pon un ritual de tu inferno en tu mano,','Los driodes viajan incansablemente hasta encontrar la sabiduría perdida en el inferno.','2',61,'','',2,3,2),(36,'Efe, pionero de la rebelión','(2): Pon en juego una ficha de objeto (Locura) Bot con la habilidad \"Destruir Bot: Remueve 1 contador de daño sobre el cyborg objetivo\".\nGirar: todos tus bots son aliados 1 FUE, 1 RES hasta el final del turno.','','5',62,'3','4',2,3,4),(37,'Hoja de la pereza','','','3',63,'-3','-0',2,3,1),(38,'Inmovilidad demencial','Cancela la carta objetivo de coste 2 o menos. Si hay Thanatos, en vez de eso, cancela la carta objetivo de coste 4 o menos.','Por más fácil que parecía, la demencia imposibilitó la tarea.','2',64,'','',2,3,1),(39,'Jemo, la ciudad flotante','Al comienzo de la Restauración de cada demonio, Jemo hace X puntos de daño a ese demonio, donde X es 3 menos la cantidad de cartas en su mano.','La ignorancia es penada por la ley.','',65,'','',2,3,3),(40,'Luminium líquido','En cuanto Luminium líquido entre en juego, puedes elegir un objeto en juego. Si lo haces, Luminium líquido entra en juego como una copia de ese objeto.','\"La grandeza de cada idea, está limitada por la grandeza de su dueño.\" -Mímire','3',66,'','',2,3,3),(41,'Minotanke','Minotanke obtiene la habilidad de halo mientras haya Thanatos','','5',67,'5','3',2,3,2),(42,'Nano-revólver de implosión','Siempre que un ritual tenga como único objetivo al aliado equipado, puedes cambiar el objetivo de ese ritual.','\"Luego de esa asombrosa maniobra esquiva, no le resultó difícil apuntar a su enemigo y abrir fuego.\" Fragmento de Efe, pionero de la rebelión','4',68,'+2','+2',2,3,3),(43,'Ojo de la emboscada','Destruir Ojo de la emboscada: Gira el aliado objetivo.','Dispositivos de control, para las mentes negligentes.','1',69,'','',2,3,1),(44,'Persuadir','Gana el control del aliado enemigo objetivo hasta el final del turno. Endereza ese aliado. Gana la habilidad de furia hasta el final del turno.','\"Escúchame atentamente, sólo lo diré una vez.\" -Ubanna','3',70,'','',2,3,2),(45,'Portadora de la sabiduría','Siempre que un demonio tome una carta, puedes poner en juego una ficha de aliado vampiro 1 FUE, 1 RES.','','6',71,'5','5',2,3,3),(46,'Buscador de violencia perdida','Vinculo 0: Pon el arma de cose 3 o menos de tu Inferno en tu mano (Puedes jugar esta habilidad cuando esta carta sea puesta en el Inferno desde el mazo, pagando su coste de vinculo. Si lo haces, exiliala).','Poseen un poderoso vinculo con el Inferno','2',15,'2','1',1,4,1),(47,'Predicción oportuna','Mira y ordena las primeras tres cartas del mazo del demonio objetivo.\nVínculo (1)- Mira y ordena las primeras tres cartas de tu mazo.','\"Sí, he visto tu futuro. No, no vivirás tanto.\"','1',72,'','',2,3,1),(48,'Reflector del pánico','(1), girar: gira el aliado objetivo','De su luz surgen tus mayores miedos.','3',73,'','',2,3,2),(49,'Robar cordura','Mira la mano del demonio enemigo objetivo y elige una carta. Busca en su mazo, mano e inferno todas las copias de esa carta y exílialas. Luego, ese demonio mezcla su mazo. Exilia Robar cordura.','\"Si la inmortalidad reside en el mundo de las ideas, tu vida es mía, pues así también lo es tu mente.\" -Baggrund','4',74,'','',2,3,4),(50,'Sabios del vacío','Cuando Sabios del vacío vaya a un inferno desde el juego, cada demonio enemigo descarta una carta.','\"Extrañaré la risa, llanto y enojo de estos humanos. Bueno, tal vez no.\" -Baggrund','4',75,'3','3',2,3,2),(51,'Telépata de la esfera','Cuando Telépata de la esfera entre en juego mira la mano del demonio objetivo.','No hay secreto escondido para ellos.','2',76,'2','2',2,3,1),(52,'Terapia láser','Cancela la carta objetivo con Thanatos','\"No va a dolerte...\" -Baggrund','2',77,'','',2,3,2),(53,'Tratante de conocimiento','Siempre que Tratante de conocimiento ataque, toma 3 cartas','Verda, era el único cyborg que lejos de imitar las usanzas del lugar, había introducido su propio gusto a la población.\" Fragmento de Efe, pionero de la rebelión','4',78,'3','4',2,3,3),(54,'Visión profunda','Mira la mano del demonio objetivo, elige una carta que no sea de aliado. Luego ese demonio descarta esa carta.','\"No te preocupes, terminaré pronto.\" -Baggrund','1',79,'','',2,3,1),(55,'Yelmo de la verdad','Cuando Yelmo de la verdad entre en juego, elige un demonio. Ese demonio juega mostrando su mano mientras Yelmo de la verdad permanezca en juego.','\"No me cuentes nada, ya lo sé TODO.\" -Viden','1',80,'','',2,3,1),(56,'Mímire, coleccionapensamientos','Cuando tengas nueve o más cartas en tu mano, voltea Mímire.','\"Cada pensamiento, cada idea, me acerca más a la perfección.\"','0',5,'1','0',2,3,2),(57,'Viden, el traicionado','Cuando tengas cero cartas en tu mazo','\"¿Qué clase de engaño es este?\n¡Ahora sentirás la ira de un verdadero Demonio!\"','0',6,'99','0',2,3,2),(58,'Baggrund, especuladora frenética','Descarta tu mano: Voltea Baggrund.\n\nDescarta una carta de tu mano: Baggrund obtiene +1 FUE hasta el final del turno.','\"No necesito opciones, yo moldeo mi futuro.\"','0',4,'0','0',2,3,2),(59,'Akayra, fuego eterno','Cuando Akayra haga 5 o más puntos de daño en un turno, voltea Akayra.\n\nSiempre que juegues un ritual, Akayra obtiene +1 FUE hasta el final del turno.','\"Crees que un simple rasguño puede detenerme? ¡Yo soy la hija del fuego!\"','0',1,'0','0',2,4,2),(60,'Rasap, forjador de victorias','(0): Voltea Rasap. Juega esta habilidad sólo si controlas tres o más armas.\n\nNo puedes jugar aliados.\nTe cuesta (1) menos equipar las armas.','\"La gloria volverá a ser mía. Esta vez para siempre.\"','0',2,'0','0',2,4,2),(61,'Syv, señor del desierto','Cuando haya siete o más aliados en juego, voltea Syv.','\"Laz arenahz hemoz de ezpandir. ¡El mundo zherá nueztro!\"','0',3,'1','0',2,4,2),(62,'At Fjerne, peregrino infernal','Exilia las primeras tres cartas de tu inferno: Voltea At Fjerne.\n\nExilia las primeras tres cartas de tu inferno: At Fjerne obtiene +1 FUE hasta el final del turno.','\"Ahora que la tierra es mi hogar, la decoraré a mi gusto.\"','0',7,'0','0',2,2,2),(63,'Naamah, artífice del submundo','Exilia las primeras cinco cartas de tu mazo boca arriba: voltea Naamah.\n\nTus enemigos pueden jugar las cartas exiliadas por Naamah como si estuvieran en su mano. (Cuando las cartas dejen la cadena son puestas en tu inferno).','\"Tus niños podrán jugar con los desechos que encuentren en el submundo.\"','0',8,'1','0',2,2,2),(64,'Zulfur, patriarca de los caídos','Cuando un aliado entre en juego bajo tu control desde un inferno, puedes voltear Zulfur.\n\nCuando Zulfur sea voletado, pon el aliado objetivo de un inferno enemigo en juego bajo tu control.\n','\"No soy el creador, sólo manipulo la realidad.\"','0',9,'0','0',2,2,2),(65,'Arasunu, amo de las bestias','Siempre que juegues un aliado, si es el tercer aliado que jugaste este turno, voltea Arasunu.','\"En el número habita la fuerza.\"','0',10,'1','0',2,1,2),(66,'Dämonin, la esclavizada','Cuando controles diez o más recursos, voltea Dämonin.','\"Corran malditos infieles. Pronto seré libre y adoro la cacería.\"','0',11,'3','0',2,1,2),(67,'Zustatek, guardián del bosque','(4): Voltea Zustatek.\n\n(4): Zustatek obtiene +1 FUE hasta el final del turno.','\"Este es mi dominio, y no eres bienvenido.\"','0',12,'0','0',2,1,2),(68,'Araña gigante','','','8',115,'9','7',2,1,2),(69,'Baku señor de los reptiles','Baku no puede ser objetivo de rituales.\nGirar un aliado enderezado que controles: Gira el aliado objetivo.','¡Soy Baku!, señor de las criaturas que reptan y sisean. ¡El amo exige una ofrenda!','4',116,'2','2',2,1,4),(70,'Barahai aguerrido','Barahai aguerrido tiene la habilidad de agresor mientras permanezca enderezado.','\"Mis compañeros cuentan conmigo, debo mantenerme despierto.\"','1',117,'1','2',2,1,1),(71,'Barahai macizo','Siempre que Barahai macizo ataque, obtiene +1 FUE hasta el final del turno por cada contador de daño sobre él.','\"Anursaur, hijo de la casa del árbol sagrado, yo sanaré tu martirio.\" -Arasunu','5',118,'4','3',2,1,1),(72,'Barahai poseído','Barahai poseído obtiene +1 FUE, +1 RES y tiene la habilidad de agresor mientras haya Thanatos. Barahai poseído es un vampiro además de sus otros tipos mientras haya Thanatos.','\"Los barahai fueron los últimos en caer. El dominio de Insania era ahora total.\" -Ubanna','1',119,'1','1',2,1,2),(73,'Barahai protegido','Si Barahai protegido fuera a recibir daño de un ritual, prevén ese daño.','\"¿Eso es todo lo que tienes? ¡No me hagas reir!\"','5',120,'5','5',2,1,2),(75,'Barahai vanidoso','Furia (Puede atacar inmediatamente).\nBarahai vanidoso obtiene +1 FUE y +1 RES mientras haya Thanatos.','Siempre se destacó entre sus hermanos. Esta vez de la forma incorrecta.','6',122,'4','4',2,1,2),(76,'Brújula de Estari','(2), girar: Busca en tu mazo un escenario, muéstralo y ponlo en tu mano. Luego, mezcla tu mazo.','\"Hacia el este, por el paso de la ciudad funeraria hasta la costa; ese es nuestro camino.\" -Estari','1',123,'','',2,1,3),(77,'Caminante de Isgar','Vínculo (0)- Endereza el recurso objetivo.','\"En la ciudad sagrada, nuestra unión con la naturaleza obtiene su esplendor.\"','3',124,'2','3',2,1,2),(78,'Campeón Barahai','','','2',125,'2','2',2,1,1),(79,'Comandante de Trion','Girar un aliado enderezado que controles: Comandante de Trion obtiene +1 FUE hasta el final del turno.','','4',126,'2','4',2,1,2),(80,'Concilio púrpura','Busca en tu mazo un aliado y muéstralo. Luego, mezcla tu mazo y pon esa carta en la parte superior.','-\"Maestro...\"\n-\"Te ofrecemos las reliquias extraviadas...\"','1',127,'','',2,1,3),(81,'Cortes de rabia','El aliado objetivo obtiene +1 FUE y gana la habilidad de furia hasta el final del turno.\nRecobrar (1).','Los barahai se preparan para la guerra, con un rito de dolor y gloria.','1',128,'','',2,1,1),(83,'Daga despiadada','(1), Girar: Pon un contador de daño sobre el aliado de menor resistencia.','No importa el portador, el resultado siempre será el mismo.','3',130,'','',2,1,2),(84,'Dragón felino','','','6',131,'7','7',2,1,1),(85,'Espejo de colombus','Destruir Espejo de colombus: Destruye el objeto objetivo.','El eco de la calavera romperse en mil pedazos pudo sentirse... pero no escucharse.','1',132,'','',2,1,1),(87,'Barahai rabiademencia','Furia (Puede atacar inmediatamente).\nBarahai rabiademencia obtiene +2 RES mientras haya Thanatos.','El pulso de muerte y destrucción invade la Tierra. El mundo cambiará para siempre.','4',121,'3','1',2,1,1),(88,'Crecimiento demencial','Agrega 2 energ´´ias. Si hay Thanatos, en vez de eso, agrega 3 energías.','\"Unos ojos nos observaron mientras pensábamos. Sin embargo, éramos las únicas criaturas en kilómetros.\" Caminante de Trion','1',129,'','',2,1,1),(89,'Grito de batalla','Endereza todos los aliados que controles, obtienen +3 FUE hasta el final del turno.','\"¡Avanzad sin piedad!\" -Dakal, regente de Trion','8',133,'','',2,1,4),(90,'Buscador hoja ardiente','Vinculo (1): Destruye el arma objetivo.','\"Fuego sagrado, guìa mi hoja hacia nuestros enemigos.\"','3',13,'2','2',2,4,1),(91,'Cabo de entrenamiento','Cuando Cabo de entrenamiento entre en juego, pon en juego 1 ficha de aliado Kirch FUE 1 RES 1 con la habilidad de furia.','Maltratos, rabia y sangre. El trabajo soñado al que aspira todo niño de la arena.','3',14,'2','2',2,4,1),(92,'Cachorros kirck','','','2',15,'3','1',2,4,1),(93,'Caminante de los silos','Vínculo(0): Tu demonio hace 1 punto de daño al demonio objetivo.','\"Su voz me susurra sobre tiempos antiguos, donde el fuego era vida, y ella su dueña.\"','3',17,'2','3',2,4,2),(94,'Chispa infortunada','Tu demonio hace 1 punto de daño al aliado o demonio objetivo. Recobrar(1).','\"Nota mental: no conjurar rituales desconocidos en la biblioteca.\" -Bibliotecario de los silos','1',18,'','',2,4,1),(95,'Cimitarra de cristal','Siempre que una ficha de aliado entre en juego bajo tu control, puedes equipar Cimitarra de cristal a esa ficha.','\"El arma silba como una tormenta de arena en la noche.\" Fragmento del Imperio de la arena roja','4',19,'+2','+2',2,4,3),(96,'Cráneos de hierro fundido','Destruir Cráneos de hierro fundido: Tu demonio hace 1 punto de daño al aliado o demonio objetivo.','Los portadores de los cráneos suelen perder sus huellas dactilares a temprana edad.','2',20,'+1','+1',2,4,2),(97,'Hoja de la ira','','','3',21,'+3','+0',2,4,1),(98,'Hornos de hierro','Siempre que un aliado reciba daño, Hornos de hierro hace esa misma cantidad de daño al controlador de ese aliado.','Las historias de quienes han muerto aquí hieren a quienes las oyen.','3',22,'','',2,4,4),(99,'Incendiar','Tu demonio hace 1 punto de daño a cada aliado.','Fuego: causa y solucion de cada problema Nungnarh.','1',23,'','',2,4,2),(100,'Incitación de Ubana','Tus aliados obtienen +1 FUE hasta el final del turno. Recobrar(5).','','3',24,'','',2,4,2),(101,'Infantes de vanguardia','Furia(Puede atacar inmediatamente).','Enviar a los niños en la batalla, puede sonar cruel... sobretodo para los enemigos difuntos.','2',25,'2','2',2,4,1),(102,'Inyección sangrearena','Destruir Inyección sangrearena: Pon en juego una ficha de aliado Kirch 1FUE, 1RES con la habilidad de furia.','Sangre derramada en batalla, materia prima para la cirugía avanzada.','1',26,'','',2,4,1),(103,'Kirch berserker','Siempre que Kirch berserker ataque, obtiene +2 FUE hasta el final del turno.','La versatilidad del luminium liquido, lo transforma sin duda en el mineral más codiciado.','3',27,'2','2',2,4,1),(104,'Kirch fuegodemencia','Cuando Kirch fuegodemencia entre en juego, si hay Thanatos, hace 1 punto de daño al aliado objetivo.','Los débiles sucumbieron ante el pulso de destruccion... Nunca mas los llamarán de ese modo.','3',28,'2','2',2,4,1),(105,'Kirch persistente','Cuando Kirch persistente vaya a un infierno desde el juego, pon en juego una ficha de aliado Kirch 1 FUE, 1 RES con la habilidad de furia.','Cuando el espíritu se niega al Infierno.','2',29,'2','1',2,4,2),(106,'Kirch poseido','Kirch poseído obtiene +1 FUE, +1 RES y tiene la habilidad de furia mientras haya Thanatos. Kirch poseído es un vampiro ademàs de sus otros tipos mientras haya Thanatos.','','1',30,'1','1',2,4,2),(107,'Kirch solitario','','','0',31,'1','1',2,4,1),(108,'Lágrimas de Syb','Destruye todas las cartas giradas.','','7',32,'','',2,4,4),(109,'Luminiscencia demencial','Tu demonio hace 1 punto de daño al aliado o demonio objetivo. Si hay Thanatos, en ves de eso, hace 3 puntos de daño al aliado o demonio objetivo.','\"La gran llama verde ha decidido tu destino.\"','1',33,'','',2,4,1),(110,'Maldición de sangre','Destruye el arma o recurso objetivo.','\"Sangre de cirugías fallidas. El maestro Tod es un gran proveedor.\"','4',34,'','',2,4,1),(111,'Manual de demolición','(0): Destruye un recurso que controles','Instrucciones tan simples, que cualquier niño Nungnarh podría llevarlas a cabo.','1',35,'','',2,4,1);
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_artist`
--

DROP TABLE IF EXISTS `cards_artist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_artist` (
  `ARTIST_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`ARTIST_ID`,`IDX`),
  KEY `FK6EB7CBC3128518F3` (`ARTIST_ID`),
  KEY `FK6EB7CBC3F6A698C4` (`elt`),
  CONSTRAINT `FK6EB7CBC3128518F3` FOREIGN KEY (`ARTIST_ID`) REFERENCES `cards` (`CARD_ID`),
  CONSTRAINT `FK6EB7CBC3F6A698C4` FOREIGN KEY (`elt`) REFERENCES `artists` (`ARTIST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_artist`
--

LOCK TABLES `cards_artist` WRITE;
/*!40000 ALTER TABLE `cards_artist` DISABLE KEYS */;
INSERT INTO `cards_artist` VALUES (21,1,0),(51,1,0),(92,1,0),(93,1,0),(65,3,0),(25,4,0),(50,4,0),(63,4,0),(105,4,0),(44,5,0),(75,5,0),(83,5,0),(100,5,0),(59,6,0),(62,6,0),(87,6,0),(23,9,0),(55,9,0),(31,10,0),(37,10,0),(76,10,0),(97,10,0),(104,10,0),(106,10,0),(28,11,0),(33,12,0),(108,12,0),(111,12,0),(94,13,0),(103,13,0),(27,14,0),(35,14,0),(48,14,0),(81,14,0),(107,14,0),(25,16,1),(50,16,1),(61,16,1),(63,16,1),(71,16,1),(73,16,1),(71,17,0),(46,18,0),(54,18,0),(68,18,0),(30,20,0),(32,20,0),(64,20,0),(69,20,0),(80,20,0),(96,20,0),(105,20,1),(70,21,0),(85,21,0),(26,22,1),(109,22,1),(43,23,0),(34,24,0),(42,24,0),(72,24,0),(110,24,0),(24,25,1),(22,26,0),(101,26,0),(29,27,1),(58,27,1),(67,27,1),(84,27,1),(56,29,0),(38,30,0),(57,30,0),(60,30,0),(36,31,0),(52,32,0),(53,32,0),(78,33,0),(39,35,1),(45,35,0),(49,35,1),(77,35,0),(79,35,1),(88,35,0),(90,35,0),(39,36,0),(29,37,0),(58,37,0),(67,37,0),(84,37,0),(89,38,0),(91,38,0),(61,39,0),(73,39,0),(40,40,1),(66,40,0),(95,41,0),(98,41,0),(102,41,0),(47,42,0),(99,42,0),(24,43,0),(26,43,0),(49,43,0),(109,43,0),(40,44,0),(41,46,0),(79,47,0);
/*!40000 ALTER TABLE `cards_artist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_subtype`
--

DROP TABLE IF EXISTS `cards_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_subtype` (
  `SUBTYPE_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`SUBTYPE_ID`,`IDX`),
  KEY `FK2497D07E1E539E0` (`SUBTYPE_ID`),
  KEY `FK2497D07E9A549F57` (`elt`),
  CONSTRAINT `FK2497D07E1E539E0` FOREIGN KEY (`SUBTYPE_ID`) REFERENCES `cards` (`CARD_ID`),
  CONSTRAINT `FK2497D07E9A549F57` FOREIGN KEY (`elt`) REFERENCES `subtypes` (`SUBTYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_subtype`
--

LOCK TABLES `cards_subtype` WRITE;
/*!40000 ALTER TABLE `cards_subtype` DISABLE KEYS */;
INSERT INTO `cards_subtype` VALUES (68,2,0),(70,4,0),(71,4,0),(72,4,0),(73,4,0),(75,4,0),(78,4,0),(87,4,0),(84,5,0),(25,6,0),(28,6,0),(29,6,0),(30,6,0),(36,6,0),(41,6,0),(53,6,0),(15,11,0),(24,11,0),(46,11,0),(50,11,0),(51,11,0),(77,11,0),(79,11,0),(90,11,0),(91,11,0),(93,11,0),(101,11,0),(92,13,0),(103,13,0),(104,13,0),(105,13,0),(106,13,0),(107,13,0),(69,16,0),(45,17,0),(21,18,0),(36,19,1),(69,19,1);
/*!40000 ALTER TABLE `cards_subtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_supertype`
--

DROP TABLE IF EXISTS `cards_supertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_supertype` (
  `SUPERTYPE_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`SUPERTYPE_ID`,`IDX`),
  KEY `FK2883BB391C87AF12` (`elt`),
  KEY `FK2883BB39E027C045` (`SUPERTYPE_ID`),
  CONSTRAINT `FK2883BB391C87AF12` FOREIGN KEY (`elt`) REFERENCES `supertypes` (`SUPERTYPE_ID`),
  CONSTRAINT `FK2883BB39E027C045` FOREIGN KEY (`SUPERTYPE_ID`) REFERENCES `cards` (`CARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_supertype`
--

LOCK TABLES `cards_supertype` WRITE;
/*!40000 ALTER TABLE `cards_supertype` DISABLE KEYS */;
INSERT INTO `cards_supertype` VALUES (22,2,0),(25,2,0),(28,2,0),(32,2,0),(33,2,0),(38,2,0),(41,2,0),(47,2,0),(52,2,0),(68,2,0),(78,2,0),(80,2,0),(94,2,0),(100,2,0),(109,2,0),(31,3,0),(36,3,0),(42,3,0),(45,3,0),(69,3,0),(95,3,0),(6,4,0),(56,4,0),(57,4,0),(58,4,0),(59,4,0),(60,4,0),(61,4,0),(62,4,0),(63,4,0),(64,4,0),(65,4,0),(66,4,0),(67,4,0);
/*!40000 ALTER TABLE `cards_supertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_type`
--

DROP TABLE IF EXISTS `cards_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_type` (
  `TYPE_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`TYPE_ID`,`IDX`),
  KEY `FK7AF46AB6F0503CB7` (`elt`),
  KEY `FK7AF46AB6BD6D8020` (`TYPE_ID`),
  CONSTRAINT `FK7AF46AB6BD6D8020` FOREIGN KEY (`TYPE_ID`) REFERENCES `cards` (`CARD_ID`),
  CONSTRAINT `FK7AF46AB6F0503CB7` FOREIGN KEY (`elt`) REFERENCES `types` (`TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_type`
--

LOCK TABLES `cards_type` WRITE;
/*!40000 ALTER TABLE `cards_type` DISABLE KEYS */;
INSERT INTO `cards_type` VALUES (15,1,0),(21,1,0),(24,1,0),(25,1,0),(28,1,0),(29,1,0),(30,1,0),(36,1,0),(41,1,0),(45,1,0),(46,1,0),(50,1,0),(51,1,0),(53,1,0),(68,1,0),(69,1,0),(70,1,0),(71,1,0),(72,1,0),(73,1,0),(75,1,0),(77,1,0),(78,1,0),(79,1,0),(84,1,0),(87,1,0),(90,1,0),(91,1,0),(92,1,0),(93,1,0),(101,1,0),(103,1,0),(104,1,0),(105,1,0),(106,1,0),(107,1,0),(37,2,0),(42,2,0),(95,2,0),(96,2,0),(97,2,0),(22,3,0),(26,3,0),(27,3,0),(32,3,0),(33,3,0),(38,3,0),(44,3,0),(47,3,0),(49,3,0),(52,3,0),(54,3,0),(80,3,0),(81,3,0),(88,3,0),(89,3,0),(94,3,0),(99,3,0),(100,3,0),(108,3,0),(109,3,0),(110,3,0),(23,4,0),(31,4,0),(34,4,0),(35,4,0),(40,4,0),(43,4,0),(48,4,0),(55,4,0),(76,4,0),(83,4,0),(85,4,0),(98,4,0),(102,4,0),(111,4,0),(39,5,0);
/*!40000 ALTER TABLE `cards_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `decks`
--

DROP TABLE IF EXISTS `decks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decks` (
  `DECK_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `DEMON_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`DECK_ID`),
  UNIQUE KEY `name` (`name`),
  KEY `FK3DEA0CA236166CF` (`DEMON_ID`),
  CONSTRAINT `FK3DEA0CA236166CF` FOREIGN KEY (`DEMON_ID`) REFERENCES `cards` (`CARD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decks`
--

LOCK TABLES `decks` WRITE;
/*!40000 ALTER TABLE `decks` DISABLE KEYS */;
INSERT INTO `decks` VALUES (1,'Nuevo Mazo',6);
/*!40000 ALTER TABLE `decks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `decks_cards`
--

DROP TABLE IF EXISTS `decks_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decks_cards` (
  `CARD_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`CARD_ID`,`IDX`),
  KEY `FK6FCEABAEF048288D` (`elt`),
  KEY `FK6FCEABAE114B7E23` (`CARD_ID`),
  CONSTRAINT `FK6FCEABAE114B7E23` FOREIGN KEY (`CARD_ID`) REFERENCES `decks` (`DECK_ID`),
  CONSTRAINT `FK6FCEABAEF048288D` FOREIGN KEY (`elt`) REFERENCES `cards` (`CARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decks_cards`
--

LOCK TABLES `decks_cards` WRITE;
/*!40000 ALTER TABLE `decks_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `decks_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expansions`
--

DROP TABLE IF EXISTS `expansions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expansions` (
  `EXPANSION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EXPANSION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expansions`
--

LOCK TABLES `expansions` WRITE;
/*!40000 ALTER TABLE `expansions` DISABLE KEYS */;
INSERT INTO `expansions` VALUES (1,'Despertar'),(2,'Insania');
/*!40000 ALTER TABLE `expansions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frequencies`
--

DROP TABLE IF EXISTS `frequencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frequencies` (
  `FREQUENCY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FREQUENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frequencies`
--

LOCK TABLES `frequencies` WRITE;
/*!40000 ALTER TABLE `frequencies` DISABLE KEYS */;
INSERT INTO `frequencies` VALUES (1,'Común','fcffff'),(2,'Infrecuente','e91b25'),(3,'Rara','fff300'),(4,'Épica','02aeee');
/*!40000 ALTER TABLE `frequencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pathways`
--

DROP TABLE IF EXISTS `pathways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pathways` (
  `PATHWAY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `primaryColor` varchar(255) DEFAULT NULL,
  `secondColor` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`PATHWAY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pathways`
--

LOCK TABLES `pathways` WRITE;
/*!40000 ALTER TABLE `pathways` DISABLE KEYS */;
INSERT INTO `pathways` VALUES (1,'Poder','007c00','004d00','En las húmedas y selváticas tierras del Poder, la presencia de los Demonios potenció el crecimiento de toda vegetación, sin conocer límite. Los humanos aprendieron a adaptarse construyendo sus viviendas alrededor y dentro de grandes árboles. Entrenaron a los animales salvajes que habitaban, y los prepararon para la guerra.'),(2,'Muerte','9f2fa9','790483','En las góticas ciudades de la Muerte, los humanos continúan con sus linajes medievales, compartiendo sus secretos y hechicería a su oscura progenie. Poseen un profundo vínculo con la vida y la muerte, realizando rituales en ofrenda a los Demonios. Tratar de conseguir su conocimiento puede traducirse en participar activamente de un ritual de sacrificio...'),(3,'Locura','425bd9','0c27b0','En las actualizadas ciudades de la Locura, el conocimiento y el tiempo es la moneda de cambio. Su ambición por el control total de la física y la tecnología ha llevado a estos humanos a consumir cuanto recurso encuentran, situación que los mantiene en constante conflicto con los demás seres terrenales. Gracias a sus avanzadas infraestructuras, se protegen del mundo exterior en sus ciudades flotantes. Pero aún asi, ¿pueden protegerse del INFERNO?'),(4,'Caos','ed693b','de4c18','En las áridas y calurosas tierras del Caos, los humanos han sobrevivido a los Demonios y desarrollado una gran fortaleza. Grandes herreros y maestros del armamento, los señores del Caos aprovechan la tecnología para hacer un sinnúmero de poderosas armas y un ejército de criaturas mutantes.'),(5,'Nautral','425bd9','0c27b0','');
/*!40000 ALTER TABLE `pathways` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subtypes`
--

DROP TABLE IF EXISTS `subtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subtypes` (
  `SUBTYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SUBTYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subtypes`
--

LOCK TABLES `subtypes` WRITE;
/*!40000 ALTER TABLE `subtypes` DISABLE KEYS */;
INSERT INTO `subtypes` VALUES (1,'Abominación'),(2,'Araña'),(3,'Avatar'),(4,'Barahai'),(5,'Bestia'),(6,'Cyborg'),(7,'Diablo'),(8,'Elemental'),(9,'Encarnación'),(10,'Homunculo'),(11,'Humano'),(12,'Insecto'),(13,'Kirch'),(14,'Lobo'),(15,'Mutante'),(16,'Reptil'),(17,'Sucubo'),(18,'Tanque'),(19,'Vampiro'),(20,'Bot');
/*!40000 ALTER TABLE `subtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supertypes`
--

DROP TABLE IF EXISTS `supertypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supertypes` (
  `SUPERTYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SUPERTYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supertypes`
--

LOCK TABLES `supertypes` WRITE;
/*!40000 ALTER TABLE `supertypes` DISABLE KEYS */;
INSERT INTO `supertypes` VALUES (1,'Ilimitado'),(2,'Instántaneo'),(3,'Único'),(4,'Demonio');
/*!40000 ALTER TABLE `supertypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `types` (
  `TYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (1,'Aliado'),(2,'Arma'),(3,'Ritual'),(4,'Objeto'),(5,'Escenario');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-10-13 19:16:06
