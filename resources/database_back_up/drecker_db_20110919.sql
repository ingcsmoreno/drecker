-- MySQL dump 10.13  Distrib 5.1.57, for debian-linux-gnu (x86_64)
--
-- Host: 192.168.1.15    Database: drecker
-- ------------------------------------------------------
-- Server version	5.1.47-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `drecker`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `drecker` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `drecker`;

--
-- Table structure for table `artists`
--

DROP TABLE IF EXISTS `artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artists` (
  `ARTIST_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ARTIST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artists`
--

LOCK TABLES `artists` WRITE;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
INSERT INTO `artists` VALUES (1,'Adrian','Kim'),(2,'Adrian','Zotto'),(3,'Alberto','Aprea'),(4,'Alejandro','Machado'),(5,'Brian','Miroglio'),(6,'Damian','Graff'),(7,'Danilo','Garcia Medrano'),(8,'Dario','Reyes'),(9,'Edgar','Machiavello'),(10,'Emmanuel','Bou'),(11,'Esteban','CamposMillapell'),(12,'Fabian Alberto','Gonzales'),(13,'Fabian Mayat','Aguilar'),(14,'Felipe','Araya'),(15,'Fernando','Morales'),(16,'Franco','Riesco'),(17,'Gaston','Real'),(18,'Gaston','Zubeldia'),(19,'Julian','Nobile'),(20,'German','Nobile'),(21,'Guillermo','Villareal'),(22,'Gustavo','Santome'),(23,'Hans','Lobos'),(24,'Ignacio','Bustos'),(25,'Ivanna','Matilla'),(26,'Juan Manuel','Tumburus'),(27,'Juan Martinez','Pinilla'),(28,'Leonardo','Vallejos'),(29,'Lizzo',''),(30,'Luciano','Comizzo'),(31,'Luciano','Fleitas'),(32,'Manuela','Mauregui'),(33,'Matias','Montenegro'),(34,'MeKenzie','Martin'),(35,'Nicolas','Fernandez'),(36,'Pablo','Gorigoitia'),(37,'Paulo','Barrios'),(38,'Roberto','Viacava'),(39,'Rodolfo','Buscaglia'),(40,'Sandro','Rybak'),(41,'Santiago','Calleriza'),(42,'Sara','Villegas'),(43,'Sergio','Tarquini'),(44,'Stryke',''),(45,'Tim','Keil'),(46,'Tomas','Aira'),(47,'Veronica','Vallejos');
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
  `CARD_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `effect` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `str` varchar(255) DEFAULT NULL,
  `res` varchar(255) DEFAULT NULL,
  `EXPANSION_ID` bigint(20) DEFAULT NULL,
  `PATHWAY_ID` bigint(20) DEFAULT NULL,
  `FREQUENCY_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`CARD_ID`),
  KEY `FK3CEEF4348B7A2AA` (`PATHWAY_ID`),
  KEY `FK3CEEF43780F816A` (`FREQUENCY_ID`),
  KEY `FK3CEEF433363228A` (`EXPANSION_ID`),
  CONSTRAINT `FK3CEEF433363228A` FOREIGN KEY (`EXPANSION_ID`) REFERENCES `expansions` (`EXPANSION_ID`),
  CONSTRAINT `FK3CEEF4348B7A2AA` FOREIGN KEY (`PATHWAY_ID`) REFERENCES `pathways` (`PATHWAY_ID`),
  CONSTRAINT `FK3CEEF43780F816A` FOREIGN KEY (`FREQUENCY_ID`) REFERENCES `frequencies` (`FREQUENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (1,'Akayra, belleza ignea','Cuando Akayra haga 5 o mas daño en un turno, volteala.','\"Mirame a los ojos. Observa tu mundo consumirse en llamas.\"',NULL,1,'1',NULL,1,4,1),(2,'Narghot, el irracional','Destruir cuatro recursos enderezados que controles: Voltea Narghot.','Destruir, necesito destruir… ¡Busquen más dominios para devastar!',NULL,2,'2',NULL,1,4,1),(3,'Syv, padre de la desgracia','Cuando controles siete o más aliados, voltea Syv.','\"La horaz ha zhegado, miz pequeñoz. ¡Ezte ez nueztro momentoz!\"',NULL,3,'3',NULL,1,4,1),(4,'Baggrund, agotamentes','Cuando tus enemigos no tengan cartas en su mano, voltea Baggrund.','\"Cuando no tienes opciones, no tienes futuro.\"',NULL,4,'3',NULL,1,3,1),(5,'Faradt, consumevoluntades','Girar cuatro aliados enderezados que controles: Voltea Fáradt.','\"La energía proviene de cualquier lado. Algunas criaturas mediocres por ejemplo.\"',NULL,5,'1',NULL,1,3,1),(6,'Viden, el omnisciente','Cuando tu mazo tenga quince o menos cartas, voltea Viden.','\"Conozco el día que perecerás. Adivinaste.\"',NULL,6,'2',NULL,1,3,1),(7,'At Fjerne, el desterrado','Exiliar las primeras diez cartas de tu Inferno: Voltea At Fjerne.','\"Destruiré mi hogar, para encontrar uno nuevo.\"',NULL,7,'1',NULL,1,2,1),(8,'Naamah, escultor de carne',NULL,NULL,NULL,8,'2',NULL,1,2,1),(9,'Unwaj, nacida de la muerte',NULL,NULL,NULL,9,'3',NULL,1,2,1),(10,'Arasunu, relampago de vida',NULL,NULL,NULL,10,'1',NULL,1,1,1),(11,'Lapatt, comandante infernal',NULL,NULL,NULL,11,'3',NULL,1,1,1),(12,'Zustatek, sabio espectante',NULL,NULL,NULL,12,'2',NULL,1,1,1),(13,'Arder en llamas','Tu demonio hace 2 daño a cada aliado y a cada demonio.','\"No hay mejor manera de empezar el dia.\" -Akayra','3',13,NULL,NULL,1,4,2),(14,'Aullido de la manada','Pon una ficha de aliado caos Kirch 1 FUE, 1 RES en juego por cada ficha que controles.','Los Kirchs sin amo vagan sin rumbo por el Gran desierto, hasta que su instinto los vuelve a reunir.','4',14,NULL,NULL,1,4,3),(15,'Buscador de violencia perdida','Vinculo 0: Pon el arma objetivo de coste 3 o menos de tu Inferno en tu mano.','Poseen un vinculo poderoso con el Inferno.','2',15,'2','1',1,4,1),(16,'Alterar el destino','Busca en tu mazo una carta con vínculo y muéstrala. Luego mezcla tu mazo y pon esa carta en la parte superior.\nRecobrar 1 (Puedes jugar esta carta desde tu Inferno, pagando su coste de recobrar. Si lo haces, exíliala).','','1',117,'','',1,5,2),(17,'Amaru','','','7',118,'7','7',1,5,1),(18,'Aniquilar','Destruye la carta objetivo.','Perplejos los caminantes observaron, cómo todo lo que conocían se reducía a escombros.','5',119,'','',1,5,3),(19,'Arrasar las ruinas','Exilia hasta tres cartas objetivo de un Inferno.','\"Esta civilización nunca existió.\" -At Fjerne','1',120,'','',1,5,1),(20,'Carnada kirch','Agresor (Tu enemigo debe proponer un aliado con agresor como defensor).','\"Hermozo hijo mioz, en tu zacrifizio ezta la victoriaz.\"-Syv','1',17,'1','1',1,4,1),(21,'Acorazado de cobre','Protector (Puedes girar este aliado y hacer que defienda al atacante, en vez del defensor propuesto).','La ciudad debe crecer, el bosque deberá buscar otro hogar.','4',47,'3','3',2,3,1),(22,'Actualizar información','Busca en tu mazo un ritual y muéstralo. Luego, mezcla tu mazo y pon esa carta en la parte superior.','\"Los implantes del tórax parecieron responder a alguna orden transmitida y abrieron un compartimiento circular en el abdómen.\" Fragmento de Efe, pionero de la rebelión.','1',48,'','',2,3,3),(23,'Bomba de oscuridad','Cuando bomba de oscuridad entre en juego, toma una carta.\nDestruir Bomba de oscuridad: El aliado objetivo pierde todas sus habilidades hasta el final del turno.','\"El artilugio los tomó por sorpresa, y no tuvieron tiempo de repelerlo.\" Fragmentos de Efe, pionero de la rebelión.','2',49,'','',2,3,1),(24,'Caminante solitario','Vínculo (1)- El demonio objetivo descarta una carta al azar.','\"¿Me harías una lectura? Sé que es un privilegio de los veneradores, ¡Pero puedo pagarte una cifra voluminosa!\" -Aspen, caminante solitario.','3',50,'2','3',2,3,2),(25,'Campeón cyborg','','','3',51,'2','4',2,3,1),(26,'Conocimiento acelerado','Toma dos cartas, luego mezcla una carta de tu mano en tu mazo.\nRecobrar (3).','\"Filtrando información, el cerebro humano es muy imperfecto.\" -Mímire','2',52,'','',2,3,1),(27,'Cubrir el acceso','Voltea el demonio objetivo si se encuentra en su forma terrenal.','\"Es un ritual sencillo. No lo he probado aún. Pero sospecho que con la ayuda de su maginficencia podría abrir un portal tan grande como para hacer pasar por él a todo Jemo.\" -Efe, pionero de la rebelión','4',53,'','',2,3,3),(28,'Cyborg paralizador','Cuando Cyborg paralizador entre en juego, si hay Thanatos, el aliado objetivo pierde todas sus habilidades hasta el final del turno.','','2',54,'1','3',2,3,1),(29,'Cyborg poseído','Cyborg poseído obtiene +1 FUE, +1 RES y tiene la habilidad de halo mientras haya Thanatos.\nCyborg poseído es un vampiro además de sus otros tipos mientras haya Thanatos.','Cuando la demencia se apodera de la locura, los resultados no son alentadores.','1',55,'1','1',2,3,2),(30,'Defensor melee','Halo (No puede ser objetivo de cartas o habilidades de un enemigo).','La segunda barrera de protección de Jemo.','1',56,'1','1',2,3,1),(31,'Delos, runa del pasado','Los demonios juegan mostrando la carta superior de su mazo.\nGirar: El demonio objetivo mezcla su mazo.','\"Las aguas del río pueden traer valiosos objetos si se aprende a manipular sus corrientes.\" -Mímire','3',57,'','',2,3,3),(32,'Desintegrar','Mezcla el aliado objetivo en el mazo de su demonio.','\"¿Crees que en algún lugar de la tierra estarías a salvo de la red de Viden?\" Fragmento de Efe, pionero de la rebelión','3',58,'','',2,3,3),(33,'Destilar ideas','Pon el recurso objetivo que controles en tu mano.\nRecobrar (2).','\"La materia es energía, al igual que el conocimiento.\" -Mímire','2',59,'','',2,3,2),(34,'Domo de energía','Siempre que un aliado enemigo ataque a tu demonio, su controlador puede pagar (2). Si no lo hace, cancela el combate.','\"El domo de defensa estaba encendido, cubriendo la ciudad con una capa de energía que hacía imposible su entrada.\" Fragmento de Efe, pionero de la rebelión','3',60,'','',2,3,4),(35,'Droide explorador','Destruir Droide explorador: Pon un ritual de tu inferno en tu mano,','Los driodes viajan incansablemente hasta encontrar la sabiduría perdida en el inferno.','2',61,'','',2,3,2),(36,'Efe, pionero de la rebelión','(2): Pon en juego una ficha de objeto (Locura) Bot con la habilidad \"Destruir Bot: Remueve 1 contador de daño sobre el cyborg objetivo\".\nGirar: todos tus bots son aliados 1 FUE, 1 RES hasta el final del turno.','','5',62,'3','4',2,3,4),(37,'Hoja de la pereza','','','3',63,'-3','-0',2,3,1),(38,'Inmovilidad demencial','Cancela la carta objetivo de coste 2 o menos. Si hay Thanatos, en vez de eso, cancela la carta objetivo de coste 4 o menos.','Por más fácil que parecía, la demencia imposibilitó la tarea.','2',64,'','',2,3,1),(39,'Jemo, la ciudad flotante','Al comienzo de la Restauración de cada demonio, Jemo hace X puntos de daño a ese demonio, donde X es 3 menos la cantidad de cartas en su mano.','La ignorancia es penada por la ley.','',65,'','',2,3,3),(40,'Luminium líquido','En cuanto Luminium líquido entre en juego, puedes elegir un objeto en juego. Si lo haces, Luminium líquido entra en juego como una copia de ese objeto.','\"La grandeza de cada idea, está limitada por la grandeza de su dueño.\" -Mímire','3',66,'','',2,3,3),(41,'Minotanke','Minotanke obtiene la habilidad de halo mientras haya Thanatos','','5',67,'5','3',2,3,2),(42,'Nano-revólver de implosión','Siempre que un ritual tenga como único objetivo al aliado equipado, puedes cambiar el objetivo de ese ritual.','\"Luego de esa asombrosa maniobra esquiva, no le resultó difícil apuntar a su enemigo y abrir fuego.\" Fragmento de Efe, pionero de la rebelión','4',68,'+2','+2',2,3,3),(43,'Ojo de la emboscada','Destruir Ojo de la emboscada: Gira el aliado objetivo.','Dispositivos de control, para las mentes negligentes.','1',69,'','',2,3,1),(44,'Persuadir','Gana el control del aliado enemigo objetivo hasta el final del turno. Endereza ese aliado. Gana la habilidad de furia hasta el final del turno.','\"Escúchame atentamente, sólo lo diré una vez.\" -Ubanna','3',70,'','',2,3,2),(45,'Portadora de la sabiduría','Siempre que un demonio tome una carta, puedes poner en juego una ficha de aliado vampiro 1 FUE, 1 RES.','','6',71,'5','5',2,3,3),(46,'Buscador de violencia perdida','Vinculo 0: Pon el arma de cose 3 o menos de tu Inferno en tu mano (Puedes jugar esta habilidad cuando esta carta sea puesta en el Inferno desde el mazo, pagando su coste de vinculo. Si lo haces, exiliala).','Poseen un poderoso vinculo con el Inferno','2',15,'2','1',1,4,1);
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_artist`
--

DROP TABLE IF EXISTS `cards_artist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_artist` (
  `ARTIST_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`ARTIST_ID`,`IDX`),
  KEY `FK6EB7CBC3128518F3` (`ARTIST_ID`),
  KEY `FK6EB7CBC3F6A698C4` (`elt`),
  CONSTRAINT `FK6EB7CBC3128518F3` FOREIGN KEY (`ARTIST_ID`) REFERENCES `cards` (`CARD_ID`),
  CONSTRAINT `FK6EB7CBC3F6A698C4` FOREIGN KEY (`elt`) REFERENCES `artists` (`ARTIST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_artist`
--

LOCK TABLES `cards_artist` WRITE;
/*!40000 ALTER TABLE `cards_artist` DISABLE KEYS */;
INSERT INTO `cards_artist` VALUES (21,1,0),(20,2,0),(25,4,0),(44,5,0),(1,6,0),(23,9,0),(31,10,0),(37,10,0),(28,11,0),(33,12,0),(17,13,0),(18,13,0),(27,14,0),(35,14,0),(16,15,0),(25,16,1),(46,18,0),(30,20,0),(32,20,0),(26,22,1),(43,23,0),(34,24,0),(42,24,0),(24,25,1),(22,26,0),(29,27,1),(38,30,0),(36,31,0),(16,35,1),(39,35,1),(45,35,0),(39,36,0),(29,37,0),(40,40,1),(19,42,0),(24,43,0),(26,43,0),(40,44,0),(41,46,0);
/*!40000 ALTER TABLE `cards_artist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_subtype`
--

DROP TABLE IF EXISTS `cards_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_subtype` (
  `SUBTYPE_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`SUBTYPE_ID`,`IDX`),
  KEY `FK2497D07E1E539E0` (`SUBTYPE_ID`),
  KEY `FK2497D07E9A549F57` (`elt`),
  CONSTRAINT `FK2497D07E1E539E0` FOREIGN KEY (`SUBTYPE_ID`) REFERENCES `cards` (`CARD_ID`),
  CONSTRAINT `FK2497D07E9A549F57` FOREIGN KEY (`elt`) REFERENCES `subtypes` (`SUBTYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_subtype`
--

LOCK TABLES `cards_subtype` WRITE;
/*!40000 ALTER TABLE `cards_subtype` DISABLE KEYS */;
INSERT INTO `cards_subtype` VALUES (17,1,0),(25,6,0),(28,6,0),(29,6,0),(30,6,0),(36,6,0),(41,6,0),(15,11,0),(24,11,0),(46,11,0),(20,13,0),(45,17,0),(21,18,0),(36,19,1);
/*!40000 ALTER TABLE `cards_subtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_supertype`
--

DROP TABLE IF EXISTS `cards_supertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_supertype` (
  `SUPERTYPE_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`SUPERTYPE_ID`,`IDX`),
  KEY `FK2883BB391C87AF12` (`elt`),
  KEY `FK2883BB39E027C045` (`SUPERTYPE_ID`),
  CONSTRAINT `FK2883BB391C87AF12` FOREIGN KEY (`elt`) REFERENCES `supertypes` (`SUPERTYPE_ID`),
  CONSTRAINT `FK2883BB39E027C045` FOREIGN KEY (`SUPERTYPE_ID`) REFERENCES `cards` (`CARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_supertype`
--

LOCK TABLES `cards_supertype` WRITE;
/*!40000 ALTER TABLE `cards_supertype` DISABLE KEYS */;
INSERT INTO `cards_supertype` VALUES (16,2,0),(22,2,0),(25,2,0),(28,2,0),(32,2,0),(33,2,0),(38,2,0),(41,2,0),(31,3,0),(36,3,0),(42,3,0),(45,3,0),(1,4,0),(2,4,0),(3,4,0),(4,4,0),(5,4,0),(6,4,0),(7,4,0),(8,4,0),(9,4,0),(10,4,0),(11,4,0),(12,4,0);
/*!40000 ALTER TABLE `cards_supertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_type`
--

DROP TABLE IF EXISTS `cards_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_type` (
  `TYPE_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`TYPE_ID`,`IDX`),
  KEY `FK7AF46AB6F0503CB7` (`elt`),
  KEY `FK7AF46AB6BD6D8020` (`TYPE_ID`),
  CONSTRAINT `FK7AF46AB6BD6D8020` FOREIGN KEY (`TYPE_ID`) REFERENCES `cards` (`CARD_ID`),
  CONSTRAINT `FK7AF46AB6F0503CB7` FOREIGN KEY (`elt`) REFERENCES `types` (`TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_type`
--

LOCK TABLES `cards_type` WRITE;
/*!40000 ALTER TABLE `cards_type` DISABLE KEYS */;
INSERT INTO `cards_type` VALUES (15,1,0),(17,1,0),(20,1,0),(21,1,0),(24,1,0),(25,1,0),(28,1,0),(29,1,0),(30,1,0),(36,1,0),(41,1,0),(45,1,0),(46,1,0),(37,2,0),(42,2,0),(13,3,0),(14,3,0),(16,3,0),(18,3,0),(19,3,0),(22,3,0),(26,3,0),(27,3,0),(32,3,0),(33,3,0),(38,3,0),(44,3,0),(23,4,0),(31,4,0),(34,4,0),(35,4,0),(40,4,0),(43,4,0),(39,5,0);
/*!40000 ALTER TABLE `cards_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `decks`
--

DROP TABLE IF EXISTS `decks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decks` (
  `DECK_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `DEMON_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`DECK_ID`),
  UNIQUE KEY `name` (`name`),
  KEY `FK3DEA0CA236166CF` (`DEMON_ID`),
  CONSTRAINT `FK3DEA0CA236166CF` FOREIGN KEY (`DEMON_ID`) REFERENCES `cards` (`CARD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decks`
--

LOCK TABLES `decks` WRITE;
/*!40000 ALTER TABLE `decks` DISABLE KEYS */;
INSERT INTO `decks` VALUES (1,'Nuevo Mazo',6);
/*!40000 ALTER TABLE `decks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `decks_cards`
--

DROP TABLE IF EXISTS `decks_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decks_cards` (
  `CARD_ID` bigint(20) NOT NULL,
  `elt` bigint(20) NOT NULL,
  `IDX` int(11) NOT NULL,
  PRIMARY KEY (`CARD_ID`,`IDX`),
  KEY `FK6FCEABAEF048288D` (`elt`),
  KEY `FK6FCEABAE114B7E23` (`CARD_ID`),
  CONSTRAINT `FK6FCEABAE114B7E23` FOREIGN KEY (`CARD_ID`) REFERENCES `decks` (`DECK_ID`),
  CONSTRAINT `FK6FCEABAEF048288D` FOREIGN KEY (`elt`) REFERENCES `cards` (`CARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decks_cards`
--

LOCK TABLES `decks_cards` WRITE;
/*!40000 ALTER TABLE `decks_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `decks_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expansions`
--

DROP TABLE IF EXISTS `expansions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expansions` (
  `EXPANSION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EXPANSION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expansions`
--

LOCK TABLES `expansions` WRITE;
/*!40000 ALTER TABLE `expansions` DISABLE KEYS */;
INSERT INTO `expansions` VALUES (1,'Despertar'),(2,'Insania');
/*!40000 ALTER TABLE `expansions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frequencies`
--

DROP TABLE IF EXISTS `frequencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frequencies` (
  `FREQUENCY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FREQUENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frequencies`
--

LOCK TABLES `frequencies` WRITE;
/*!40000 ALTER TABLE `frequencies` DISABLE KEYS */;
INSERT INTO `frequencies` VALUES (1,'Común','fcffff'),(2,'Infrecuente','e91b25'),(3,'Rara','fff300'),(4,'Épica','02aeee');
/*!40000 ALTER TABLE `frequencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pathways`
--

DROP TABLE IF EXISTS `pathways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pathways` (
  `PATHWAY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `primaryColor` varchar(255) DEFAULT NULL,
  `secondColor` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`PATHWAY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pathways`
--

LOCK TABLES `pathways` WRITE;
/*!40000 ALTER TABLE `pathways` DISABLE KEYS */;
INSERT INTO `pathways` VALUES (1,'Poder','007c00','004d00','En las húmedas y selváticas tierras del Poder, la presencia de los Demonios potenció el crecimiento de toda vegetación, sin conocer límite. Los humanos aprendieron a adaptarse construyendo sus viviendas alrededor y dentro de grandes árboles. Entrenaron a los animales salvajes que habitaban, y los prepararon para la guerra.'),(2,'Muerte','9f2fa9','790483','En las góticas ciudades de la Muerte, los humanos continúan con sus linajes medievales, compartiendo sus secretos y hechicería a su oscura progenie. Poseen un profundo vínculo con la vida y la muerte, realizando rituales en ofrenda a los Demonios. Tratar de conseguir su conocimiento puede traducirse en participar activamente de un ritual de sacrificio...'),(3,'Locura','425bd9','0c27b0','En las actualizadas ciudades de la Locura, el conocimiento y el tiempo es la moneda de cambio. Su ambición por el control total de la física y la tecnología ha llevado a estos humanos a consumir cuanto recurso encuentran, situación que los mantiene en constante conflicto con los demás seres terrenales. Gracias a sus avanzadas infraestructuras, se protegen del mundo exterior en sus ciudades flotantes. Pero aún asi, ¿pueden protegerse del INFERNO?'),(4,'Caos','ed693b','de4c18','En las áridas y calurosas tierras del Caos, los humanos han sobrevivido a los Demonios y desarrollado una gran fortaleza. Grandes herreros y maestros del armamento, los señores del Caos aprovechan la tecnología para hacer un sinnúmero de poderosas armas y un ejército de criaturas mutantes.'),(5,'Nautral','425bd9','0c27b0','');
/*!40000 ALTER TABLE `pathways` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subtypes`
--

DROP TABLE IF EXISTS `subtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subtypes` (
  `SUBTYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SUBTYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subtypes`
--

LOCK TABLES `subtypes` WRITE;
/*!40000 ALTER TABLE `subtypes` DISABLE KEYS */;
INSERT INTO `subtypes` VALUES (1,'Abominación'),(2,'Araña'),(3,'Avatar'),(4,'Barahai'),(5,'Bestia'),(6,'Cyborg'),(7,'Diablo'),(8,'Elemental'),(9,'Encarnación'),(10,'Homunculo'),(11,'Humano'),(12,'Insecto'),(13,'Kirch'),(14,'Lobo'),(15,'Mutante'),(16,'Reptil'),(17,'Sucubo'),(18,'Tanque'),(19,'Vampiro'),(20,'Bot');
/*!40000 ALTER TABLE `subtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supertypes`
--

DROP TABLE IF EXISTS `supertypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supertypes` (
  `SUPERTYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SUPERTYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supertypes`
--

LOCK TABLES `supertypes` WRITE;
/*!40000 ALTER TABLE `supertypes` DISABLE KEYS */;
INSERT INTO `supertypes` VALUES (1,'Ilimitado'),(2,'Instántaneo'),(3,'Único'),(4,'Demonio');
/*!40000 ALTER TABLE `supertypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `types` (
  `TYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (1,'Aliado'),(2,'Arma'),(3,'Ritual'),(4,'Objeto'),(5,'Escenario');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-09-19 18:44:29
